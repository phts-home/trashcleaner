unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, FlatButton, StdCtrls, CheckLst, ComCtrls, Masks,
  FileCtrl, ShellCtrls, Menus, Grids, ShellAPI, IniFiles, Registry,
  ActnList;

type
  TFileLCX = Record
    RecVersion: Byte;
    RecTotalSize, RecTotalFiles, RecTotalDirs: LongInt;
    RecFile, RecSize, RecStatus: String [255];
    end;
type TMainFile = File of TFileLCX;
type
  TFMain = class(TForm)
    GroupBox1: TGroupBox;
    ChBox_tmp: TCheckBox;
    ChBox_thumbs: TCheckBox;
    ShellTreeView1: TShellTreeView;
    Edit1: TEdit;
    MainMenu1: TMainMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    N6: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    FlatButton1: TFlatButton;
    FlatButton2: TFlatButton;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    ChBox_bak: TCheckBox;
    ListView1: TListView;
    ChBox_tttt: TCheckBox;
    ChBox_ttt: TCheckBox;
    ChBox_old: TCheckBox;
    ChBox_log: TCheckBox;
    ChBox_bbb: TCheckBox;
    ChBox_wbk: TCheckBox;
    ChBox_ddd: TCheckBox;
    ChBox_swp: TCheckBox;
    StatusBar1: TStatusBar;
    N15: TMenuItem;
    PopupMenu1: TPopupMenu;
    N2: TMenuItem;
    N14: TMenuItem;
    N17: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N24: TMenuItem;
    N26: TMenuItem;
    ChBox_ccc: TCheckBox;
    N30: TMenuItem;
    N18: TMenuItem;
    N31: TMenuItem;
    N33: TMenuItem;
    EMP1: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    ChBox_dddd: TCheckBox;
    N25: TMenuItem;
    N38: TMenuItem;
    N39: TMenuItem;
    N12: TMenuItem;
    N29: TMenuItem;
    N13: TMenuItem;
    N16: TMenuItem;
    N41: TMenuItem;
    N42: TMenuItem;
    N43: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    N46: TMenuItem;
    ReadMetxt1: TMenuItem;
    N44: TMenuItem;
    FlatButton3: TFlatButton;
    N47: TMenuItem;
    N34: TMenuItem;
    N48: TMenuItem;
    N49: TMenuItem;
    N50: TMenuItem;
    ActionList1: TActionList;
    aSearch: TAction;
    aSearchTemp: TAction;
    aStop: TAction;
    aDelFile: TAction;
    aDelFilesAll: TAction;
    aExit: TAction;
    aListOpen: TAction;
    aListSave: TAction;
    aDelItem: TAction;
    aDelItemsAll: TAction;
    aSelAll: TAction;
    aShowFDlgAssoc: TAction;
    aShowFDlgFolder: TAction;
    aShowReadMetxt: TAction;
    aShowFDlgAbout: TAction;
    aInterfGrid: TAction;
    aInterfFlatScrb: TAction;
    aInterfSelRow: TAction;
    aInterfSizing: TAction;
    aOptSubFold: TAction;
    aOptDelSuc: TAction;
    aOptClBefSearch: TAction;
    aOptReset: TAction;
    aFilterFF: TAction;
    aFilterFiles: TAction;
    aFilterFold: TAction;
    aCheckboxesAll: TAction;
    aCheckboxesNone: TAction;
    aFileOpen: TAction;
    aFileInFolder: TAction;
    aFileProp: TAction;
    Panel1: TPanel;
    FlatButton4: TFlatButton;
    FlatButton5: TFlatButton;
    N27: TMenuItem;
    N28: TMenuItem;
    aSelNone: TAction;
    aSelInv: TAction;
    N32: TMenuItem;
    N40: TMenuItem;
    aOptUseRec: TAction;
    N45: TMenuItem;
    N51: TMenuItem;
    N52: TMenuItem;
    N53: TMenuItem;
    aClearRec: TAction;
    procedure Edit1Change(Sender: TObject);
    procedure ShellTreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure ListView1DblClick(Sender: TObject);
    procedure FlatButton3Click(Sender: TObject);
    procedure ListView1SelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure ListView1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ListView1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure aSearchExecute(Sender: TObject);
    procedure aSearchTempExecute(Sender: TObject);
    procedure aDelFileExecute(Sender: TObject);
    procedure aDelFilesAllExecute(Sender: TObject);
    procedure aExitExecute(Sender: TObject);
    procedure aListOpenExecute(Sender: TObject);
    procedure aListSaveExecute(Sender: TObject);
    procedure aDelItemExecute(Sender: TObject);
    procedure aDelItemsAllExecute(Sender: TObject);
    procedure aSelAllExecute(Sender: TObject);
    procedure aShowFDlgAssocExecute(Sender: TObject);
    procedure aShowFDlgFolderExecute(Sender: TObject);
    procedure aInterfGridExecute(Sender: TObject);
    procedure aInterfFlatScrbExecute(Sender: TObject);
    procedure aInterfSelRowExecute(Sender: TObject);
    procedure aInterfSizingExecute(Sender: TObject);
    procedure aOptSubFoldExecute(Sender: TObject);
    procedure aOptDelSucExecute(Sender: TObject);
    procedure aOptClBefSearchExecute(Sender: TObject);
    procedure aOptResetExecute(Sender: TObject);
    procedure aFilterFFExecute(Sender: TObject);
    procedure aFilterFilesExecute(Sender: TObject);
    procedure aFilterFoldExecute(Sender: TObject);
    procedure aCheckboxesAllExecute(Sender: TObject);
    procedure aCheckboxesNoneExecute(Sender: TObject);
    procedure aShowReadMetxtExecute(Sender: TObject);
    procedure aShowFDlgAboutExecute(Sender: TObject);
    procedure aFileOpenExecute(Sender: TObject);
    procedure aFileInFolderExecute(Sender: TObject);
    procedure aFilePropExecute(Sender: TObject);
    procedure aStopExecute(Sender: TObject);
    procedure FlatButton1Click(Sender: TObject);
    procedure Panel1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Panel1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Panel1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ListView1Resize(Sender: TObject);
    procedure aSelNoneExecute(Sender: TObject);
    procedure aSelInvExecute(Sender: TObject);
    procedure aOptUseRecExecute(Sender: TObject);
    procedure aClearRecExecute(Sender: TObject);
    procedure ListView1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure StatusBar1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure StatusBar1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    IDThread, hThread: Cardinal;
    cfg: TMemIniFile;
    OpeningFile, OpeningDir: String;
    STotalSize, SSelectedSize, SDeletedSize: String;
    TotalFiles, TotalDirs,
    SelectedFiles, SelectedDirs,
    DeletedFiles, DeletedDirs: LongInt;
    TotalSize,SelectedSize,DeletedSize: Int64;
    RightButtonItem, IndOld, IndNew: Integer;
    Moving: Boolean;
    Canceled: Boolean;

    procedure FindFiles(StartFolder: string; List: TListView;
      ScanSubFolders, TempFolders: Boolean);
    procedure DelItemFromList(Index: Integer);
    procedure CheckTotalSize;
    procedure CheckSelected;
    procedure ShowPropertiesDialog(FName: string);
    procedure ReadMCXFile (FileName: string);
    procedure WriteMCXFile (FileName: string);
    function RecycleFile(sFileName: string): Boolean;
    procedure EmptyRecycleBin;
    
    { Public declarations }
  end;

var
  FMain: TFMain;

implementation

uses uAbout, uFolders, uAssociation, uCustomM, uSearch;

{$R *.dfm}

procedure StartFindFiles;
var c: Char;
begin
FMain.FlatButton1.EnableButton:=False;
FMain.FlatButton2.EnableButton:=False;

FMain.aDelFile.Enabled:=False;
FMain.aDelFilesAll.Enabled:=False;
FMain.aDelItem.Enabled:=False;
FMain.aDelItemsAll.Enabled:=False;
FMain.aSearch.Enabled:=False;
FMain.aSearchTemp.Enabled:=False;
FMain.aCheckboxesAll.Enabled:=False;
FMain.aCheckboxesNone.Enabled:=False;
FMain.aSelAll.Enabled:=False;
FMain.aSelNone.Enabled:=False;
FMain.aSelInv.Enabled:=False;
FMain.N34.Enabled:=False;

FMain.ListView1.PopupMenu:=nil;
FMain.GroupBox1.Enabled:=False;
FMain.aStop.Enabled:=True;
if FMain.ChBox_tmp.Checked
  or FMain.ChBox_thumbs.Checked
  or FMain.ChBox_bak.Checked
  or FMain.ChBox_old.Checked
  or FMain.ChBox_ttt.Checked
  or FMain.ChBox_tttt.Checked
  or FMain.ChBox_swp.Checked
  or FMain.ChBox_ddd.Checked
  or FMain.ChBox_dddd.Checked
  or FMain.ChBox_wbk.Checked
  or FMain.ChBox_bbb.Checked
  or FMain.ChBox_log.Checked
  or FMain.ChBox_ccc.Checked then
    begin
    if FMain.aOptClBefSearch.Checked then FMain.aDelItemsAllExecute(FMain.aDelItemsAll);
    if (FMain.ShellTreeView1.Path='��� ���������')or(FMain.ShellTreeView1.Path='My computer') then
      begin
      for c:='A' to 'Z' do
        if GetDriveType(PChar(c+':\'))=DRIVE_FIXED
          then FMain.FindFiles(c+':\',FMain.ListView1, FMain.aOptSubFold.Checked, False);
      end else FMain.FindFiles(FMain.ShellTreeView1.Path,FMain.ListView1, FMain.aOptSubFold.Checked, False);
    end;
FDlgSearch.Gauge1.Progress:=100;
FDlgSearch.Label1.Visible:=True;
FDlgSearch.FlatButton1.EnableButton:=False;
FDlgSearch.FlatButton2.EnableButton:=True;
if FDlgSearch.CheckBox1.Checked then
  FDlgSearch.Close;
FMain.FlatButton1.EnableButton:=True;
FMain.FlatButton2.EnableButton:=True;
FMain.aDelFile.Enabled:=True;
FMain.aDelFilesAll.Enabled:=True;
FMain.aDelItem.Enabled:=True;
FMain.aDelItemsAll.Enabled:=True;
FMain.aSearch.Enabled:=True;
FMain.aSearchTemp.Enabled:=True;
FMain.aCheckboxesAll.Enabled:=True;
FMain.aCheckboxesNone.Enabled:=True;
FMain.aSelAll.Enabled:=True;
FMain.aSelNone.Enabled:=True;
FMain.aSelInv.Enabled:=True;
FMain.N34.Enabled:=True;

FMain.GroupBox1.Enabled:=True;
FMain.ListView1.PopupMenu:=FMain.PopupMenu1;
FMain.aStop.Enabled:=False;
end;

procedure StartFindTempFiles;
var i: Integer;
begin
FMain.FlatButton1.EnableButton:=False;
FMain.FlatButton2.EnableButton:=False;

FMain.aDelFile.Enabled:=False;
FMain.aDelFilesAll.Enabled:=False;
FMain.aDelItem.Enabled:=False;
FMain.aDelItemsAll.Enabled:=False;
FMain.aSearch.Enabled:=False;
FMain.aSearchTemp.Enabled:=False;
FMain.aCheckboxesAll.Enabled:=False;
FMain.aCheckboxesNone.Enabled:=False;
FMain.aSelAll.Enabled:=False;
FMain.aSelNone.Enabled:=False;
FMain.aSelInv.Enabled:=False;
FMain.N34.Enabled:=False;

FMain.GroupBox1.Enabled:=False;
FMain.ListView1.PopupMenu:=nil;
FMain.aStop.Enabled:=True;
if FMain.aOptClBefSearch.Checked then FMain.aDelItemsAllExecute(FMain.aDelItemsAll);
for i:=0 to FDlgFolders.ListBox1.Items.Count-1 do
  FMain.FindFiles(FDlgFolders.ListBox1.Items[i],FMain.ListView1, True, True);
FDlgSearch.Gauge1.Progress:=100;
FDlgSearch.Label1.Visible:=True;
FDlgSearch.FlatButton1.EnableButton:=False;
FDlgSearch.FlatButton2.EnableButton:=True;
if FDlgSearch.CheckBox1.Checked then
  FDlgSearch.Close;
FMain.FlatButton1.EnableButton:=True;
FMain.FlatButton2.EnableButton:=True;

FMain.aDelFile.Enabled:=True;
FMain.aDelFilesAll.Enabled:=True;
FMain.aDelItem.Enabled:=True;
FMain.aDelItemsAll.Enabled:=True;
FMain.aSearch.Enabled:=True;
FMain.aSearchTemp.Enabled:=True;
FMain.aCheckboxesAll.Enabled:=True;
FMain.aCheckboxesNone.Enabled:=True;
FMain.aSelAll.Enabled:=True;
FMain.aSelNone.Enabled:=True;
FMain.aSelInv.Enabled:=True;
FMain.N34.Enabled:=True;

FMain.GroupBox1.Enabled:=True;
FMain.ListView1.PopupMenu:=FMain.PopupMenu1;
FMain.aStop.Enabled:=False;
end;

procedure TFMain.FindFiles(StartFolder: string; List: TListView;
  ScanSubFolders, TempFolders: Boolean);
var SearchRec: TSearchRec;
    i, FindResult: Integer;
    SizeInMB, SizeInKB: Real;
    CustomMasksEx: Boolean;
begin
try
  StartFolder:=IncludeTrailingBackslash(StartFolder);
  FDlgSearch.Gauge1.Progress:=FDlgSearch.Gauge1.Progress+2;
  if FDlgSearch.Gauge1.Progress = 100 then
    FDlgSearch.Gauge1.Progress:=0;
  try
    FDlgSearch.Label4.Caption:=MinimizeName(StartFolder,FDlgSearch.Label4.Canvas,FDlgSearch.Label4.Width);
  except end;
  FindResult:=FindFirst(StartFolder + '*.*',faAnyFile,SearchRec);
    while FindResult = 0 do
      with SearchRec do
      begin
      if Attr and faDirectory <> 0 then                               //dir
        begin
        if (Name <> '.') and (Name <> '..') then
          begin
          if aFilterFold.Checked or aFilterFF.Checked then
            begin
            CustomMasksEx:=False;
            for i:=0 to FDlgCustom.CheckListBox1.Count-1 do
              if ChBox_ccc.Checked and FDlgCustom.CheckListBox1.Checked[i] and MatchesMask(Name, FDlgCustom.CheckListBox1.Items[i]) then
                begin
                CustomMasksEx:=True;
                Break;
                end;
            end;
          if TempFolders or (not aFilterFiles.Checked and (CustomMasksEx
                or (ChBox_tmp.Checked and MatchesMask(Name,'*.tmp'))
                or(ChBox_thumbs.Checked and MatchesMask(Name,'thumbs.db'))
                or(ChBox_bak.Checked and MatchesMask(Name,'*.bak'))
                or(ChBox_old.Checked and MatchesMask(Name,'*.old'))
                or(ChBox_ttt.Checked and MatchesMask(Name,'~*.*'))
                or(ChBox_tttt.Checked and MatchesMask(Name,'*.~*'))
                or(ChBox_swp.Checked and MatchesMask(Name,'*.swp'))
                or(ChBox_ddd.Checked and MatchesMask(Name,'$*.*'))
                or(ChBox_dddd.Checked and MatchesMask(Name,'*.$*'))
                or(ChBox_wbk.Checked and MatchesMask(Name,'*.wbk'))
                or(ChBox_bbb.Checked and MatchesMask(Name,'backup_of_*.*'))
                or(ChBox_log.Checked and MatchesMask(Name,'*.log'))))then
            begin
            with List.Items.Insert(0) do
              begin
              Caption:=StartFolder + Name;
              SubItems.Add('[ dir ]');
              SubItems.Add('');
              SubItems.Add('');
              end;
            Inc(TotalDirs);
            end;
          if not Canceled and ScanSubFolders then
            FindFiles(StartFolder + Name, List, ScanSubFolders, TempFolders);
          end;
        end
      else
        begin                                                     //file
        CustomMasksEx:=False;
        for i:=0 to FDlgCustom.CheckListBox1.Count-1 do
          if ChBox_ccc.Checked and FDlgCustom.CheckListBox1.Checked[i] and MatchesMask(Name, FDlgCustom.CheckListBox1.Items[i]) then
            begin
            CustomMasksEx:=True;
            Break;
            end;
        if (not aFilterFold.Checked)and(TempFolders or CustomMasksEx
          or (ChBox_tmp.Checked and MatchesMask(Name,'*.tmp'))
          or(ChBox_thumbs.Checked and MatchesMask(Name,'thumbs.db'))
          or(ChBox_bak.Checked and MatchesMask(Name,'*.bak'))
          or(ChBox_old.Checked and MatchesMask(Name,'*.old'))
          or(ChBox_ttt.Checked and MatchesMask(Name,'~*.*'))
          or(ChBox_tttt.Checked and MatchesMask(Name,'*.~*'))
          or(ChBox_swp.Checked and MatchesMask(Name,'*.swp'))
          or(ChBox_ddd.Checked and MatchesMask(Name,'$*.*'))
          or(ChBox_dddd.Checked and MatchesMask(Name,'*.$*'))
          or(ChBox_wbk.Checked and MatchesMask(Name,'*.wbk'))
          or(ChBox_bbb.Checked and MatchesMask(Name,'backup_of_*.*'))
          or(ChBox_log.Checked and MatchesMask(Name,'*.log')))then
          begin
          with List.Items.Insert(0) do
            begin
            Caption:=StartFolder + Name;
            SubItems.Add('');
            SubItems.Add('');
            SubItems.Add('');
            end;
          SizeInKB:=round(SearchRec.Size/102.4)/10;
          if SizeInKB > 1 then
            begin
            SizeInMB:=round(SearchRec.Size/10485.76)/100;
            if SizeInMB > 0.9 then
              List.Items[0].SubItems[0]:=FloatToStr(SizeInMB)+' M'
            else
              List.Items[0].SubItems[0]:=FloatToStr(SizeInKB)+' k';
            end
          else
            List.Items[0].SubItems[0]:=IntToStr(SearchRec.Size)+' b';
          List.Items[0].SubItems[2]:=IntToStr(SearchRec.Size);
          Inc(TotalFiles);
          TotalSize:=TotalSize+SearchRec.Size;
          end;
        end;
      FindResult:=FindNext(SearchRec);
      end;
  finally
  end;
FindClose(SearchRec);
CheckTotalSize;
end;

procedure TFMain.DelItemFromList(Index: Integer);
begin
if ListView1.Items[Index].SubItems[0]<>'[ dir ]' then
  begin
  Dec(TotalFiles);
  TotalSize:=TotalSize-StrToInt(ListView1.Items[Index].SubItems[2]);
  end
else Dec(TotalDirs);
ListView1.Items[Index].Delete;

if ListView1.Items.Count=0 then
  begin
  TotalFiles:=0;
  TotalDirs:=0;
  TotalSize:=0;
  end;
end;

procedure TFMain.CheckTotalSize;
var SizeInMB, SizeInKB: Real;
begin
SizeInKB:=Round(TotalSize/102.4)/10;
if SizeInKB > 1 then
  begin
  SizeInMB:=round(TotalSize/10485.76)/100;
  if SizeInMB > 0.9 then
    STotalSize:=FloatToStr(SizeInMB)+' M'
  else
    STotalSize:=FloatToStr(SizeInKB)+' k';
  end
else
  STotalSize:=IntToStr(TotalSize)+' b';
StatusBar1.Panels[2].Text:=IntToStr(TotalFiles)+'/'+IntToStr(TotalDirs)+'/'+STotalSize;
if FDlgSearch.Showing then
  begin
  FDlgSearch.Label6.Caption:=IntToStr(TotalFiles);
  FDlgSearch.Label7.Caption:=IntToStr(TotalDirs);
  FDlgSearch.Label8.Caption:=STotalSize;
  end;
end;

procedure TFMain.CheckSelected;
var i: Integer;
    SelSizeInMB, SelSizeInKB: Real;
begin
SelectedSize:=0;
SelectedFiles:=0;
SelectedDirs:=0;
for i:=0 to ListView1.Items.Count-1 do
  begin
  if ListView1.Items[i].Selected then
    begin
    if ListView1.Items[i].SubItems[0]<>'[ dir ]' then
      begin
      Inc(SelectedFiles);
      SelectedSize:=SelectedSize+StrToInt(ListView1.Items[i].SubItems[2]);
      end
    else
      Inc(SelectedDirs);
    end;
  end;

if SelectedFiles=ListView1.Items.Count then SelectedSize:=TotalSize;

SelSizeInKB:=Round(SelectedSize/102.4)/10;
if SelSizeInKB > 1 then
  begin
  SelSizeInMB:=round(SelectedSize/10485.76)/100;
  if SelSizeInMB > 0.9 then
    SSelectedSize:=FloatToStr(SelSizeInMB)+' M'
  else
    SSelectedSize:=FloatToStr(SelSizeInKB)+' k';
  end
else
  SSelectedSize:=IntToStr(SelectedSize)+' b';
StatusBar1.Panels[4].Text:=IntToStr(SelectedFiles)+'/'+IntToStr(SelectedDirs)+'/'+SSelectedSize;
end;

procedure TFMain.ReadMCXFile(FileName: string);
var Source: TMainFile;
    Item: TFileLCX;
begin
AssignFile(Source,FileName);
Reset(Source);
Read(Source,Item);
if Item.RecVersion<>16 then
  begin
  Application.MessageBox(pchar('���� ����� ����������� ������ ��� ��������'+#13
    +FileName),pchar(FMain.Caption),MB_ICONERROR+mb_OK);
  Exit;
  end;
aDelItemsAll.Execute;
TotalSize:=Item.RecTotalSize;
TotalFiles:=Item.RecTotalFiles;
TotalDirs:=Item.RecTotalDirs;
while not EOF(Source) do
  begin
  Read(Source,Item);
  with ListView1.Items.Insert(0) do
    begin
    Caption:=Item.RecFile;
    SubItems.Add(Item.RecSize);
    SubItems.Add(Item.RecStatus);
    end;
  end;
CloseFile(Source);
CheckTotalSize;
end;

procedure TFMain.WriteMCXFile(FileName: string);
var Source: TMainFile;
    Item: TFileLCX;
    i: Integer;
begin
AssignFile(Source,FileName);
Rewrite(Source);
Item.RecFile:='';
Item.RecSize:='';
Item.RecStatus:='';
Item.RecVersion:=16;
Item.RecTotalSize:=TotalSize;
Item.RecTotalFiles:=TotalFiles;
Item.RecTotalDirs:=TotalDirs;
Write(Source,Item);
Item.RecVersion:=0;
Item.RecTotalSize:=0;
Item.RecTotalFiles:=0;
Item.RecTotalDirs:=0;
for i:=ListView1.Items.Count-1 downto 0 do
  begin
  Item.RecFile:=ListView1.Items[i].Caption;
  Item.RecSize:=ListView1.Items[i].SubItems[0];
  Item.RecStatus:=ListView1.Items[i].SubItems[1];
  Write(Source,Item);
  end;
CloseFile(Source);
end;

procedure TFMain.ShowPropertiesDialog(FName: string);
var SExInfo: TSHELLEXECUTEINFO;
begin
ZeroMemory(Addr(SExInfo),SizeOf(SExInfo));
SExInfo.cbSize:=SizeOf(SExInfo);
SExInfo.lpFile:=PChar(FName);
SExInfo.lpVerb:='properties';
SExInfo.fMask:=SEE_MASK_INVOKEIDLIST;
ShellExecuteEx(Addr(SExInfo));
end;

function TFMain.RecycleFile(sFileName: string): Boolean;
var FOS: TSHFileOpStruct;
begin
try
  FillChar(FOS, SizeOf(FOS), 0);
  with FOS do
    begin
    wFunc:=FO_DELETE;
    pFrom:=PChar(sFileName+#0);
    fFlags:=FOF_ALLOWUNDO;
    end;
Result:=SHFileOperation(FOS) = 0;
except end;
end;

procedure TFMain.EmptyRecycleBin;
const
  SHERB_NOCONFIRMATION = $00000001;
  SHERB_NOPROGRESSUI = $00000002;
  SHERB_NOSOUND = $00000004;
type
  TSHEmptyRecycleBin = function(Wnd: HWND; pszRootPath: PChar; dwFlags: DWORD): HRESULT;  stdcall;
var
  SHEmptyRecycleBin: TSHEmptyRecycleBin;
  LibHandle: THandle;
begin 
  LibHandle:=LoadLibrary(PChar('Shell32.dll'));
  if LibHandle <> 0 then
    @SHEmptyRecycleBin:=GetProcAddress(LibHandle, 'SHEmptyRecycleBinA')
  else
    begin
    Application.MessageBox(PChar('Failed to load Shell32.dll'),PChar(FMain.Caption), mb_IconError+mb_OK);
    Exit;
  end;
  if @SHEmptyRecycleBin <> nil then
    SHEmptyRecycleBin(Application.Handle,nil,
      SHERB_NOCONFIRMATION or SHERB_NOPROGRESSUI or SHERB_NOSOUND);
  FreeLibrary(LibHandle);
  @SHEmptyRecycleBin:=nil;
end;



// ........Form.......

procedure TFMain.FormCreate(Sender: TObject);
begin
cfg:=TMemIniFile.Create(ExtractFilePath(ParamStr(0))+'Cleaner.ini');
Width:=cfg.ReadInteger('Form','Width',755);
Height:=cfg.ReadInteger('Form','Height',395);
Left:=cfg.ReadInteger('Form','Left',181);
Top:=cfg.ReadInteger('Form','Top',156);
Panel1.Left:=cfg.ReadInteger('Form','SepLeft',171);
  ShellTreeView1.Width:=Panel1.Left - ShellTreeView1.Left;
  Edit1.Width:=Panel1.Left - Edit1.Left;
  ListView1.Left:=Panel1.Left + Panel1.Width;
  ListView1.Width:=FMain.ClientWidth-ListView1.Left-117;


aOptUseRec.Checked:=cfg.ReadBool('Program','UseRecycleBin',False);
aOptSubFold.Checked:=cfg.ReadBool('Program','SearchInSubdir',True);
aOptDelSuc.Checked:=cfg.ReadBool('Program','DelSuccess',False);
aOptClBefSearch.Checked:=cfg.ReadBool('Program','ClearBeforeSearch',True);
aInterfGrid.Checked:=cfg.ReadBool('Program','GridLines',True);
if aInterfGrid.Checked then ListView1.GridLines:=True;
aInterfFlatScrb.Checked:=cfg.ReadBool('Program','FlatScrollbars',True);
if aInterfFlatScrb.Checked then ListView1.FlatScrollBars:=True;
aInterfSelRow.Checked:=cfg.ReadBool('Program','RowSelect',True);
if aInterfSelRow.Checked then ListView1.RowSelect:=True;

case cfg.ReadInteger('Form','WindowState',0) of
  0: FMain.WindowState:=wsNormal;
  1: FMain.WindowState:=wsMaximized;
  end;

case cfg.ReadInteger('Program','Filter',0) of
  0: aFilterFF.Execute;
  1: aFilterFiles.Execute;
  2: aFilterFold.Execute;
  end;

ChBox_tmp.Checked:=cfg.ReadBool('Checkboxes','Checkbox001',True);
ChBox_thumbs.Checked:=cfg.ReadBool('Checkboxes','Checkbox002',True);
ChBox_bak.Checked:=cfg.ReadBool('Checkboxes','Checkbox003',True);
ChBox_old.Checked:=cfg.ReadBool('Checkboxes','Checkbox004',True);
ChBox_ttt.Checked:=cfg.ReadBool('Checkboxes','Checkbox005',True);
ChBox_tttt.Checked:=cfg.ReadBool('Checkboxes','Checkbox006',True);
ChBox_swp.Checked:=cfg.ReadBool('Checkboxes','Checkbox007',True);
ChBox_ddd.Checked:=cfg.ReadBool('Checkboxes','Checkbox008',True);
ChBox_dddd.Checked:=cfg.ReadBool('Checkboxes','Checkbox009',True);
ChBox_wbk.Checked:=cfg.ReadBool('Checkboxes','Checkbox010',True);
ChBox_bbb.Checked:=cfg.ReadBool('Checkboxes','Checkbox011',True);
ChBox_log.Checked:=cfg.ReadBool('Checkboxes','Checkbox012',False);
ChBox_ccc.Checked:=cfg.ReadBool('Checkboxes','Checkbox013',False);

STotalSize:='0 b';
SDeletedSize:='0 b';
SSelectedSize:='0 b';
end;

procedure TFMain.FormClose(Sender: TObject; var Action: TCloseAction);
var ExCode: Cardinal;
    i: Integer;
    TempSt: String;
begin
if FMain.WindowState=wsNormal
  then cfg.WriteInteger('Form','WindowState',0)
  else
    if FMain.WindowState=wsMaximized then
      begin
      FMain.WindowState:=wsNormal;
      cfg.WriteInteger('Form','WindowState',1);
      end;

cfg.WriteInteger('Form','Width',Width);
cfg.WriteInteger('Form','Height',Height);
cfg.WriteInteger('Form','Left',Left);
cfg.WriteInteger('Form','Top',Top);
cfg.WriteInteger('Form','SepLeft',Panel1.Left);

cfg.WriteString('Program','Path',ShellTreeView1.Path);

cfg.WriteBool('Program','UseRecycleBin',aOptUseRec.Checked);
cfg.WriteBool('Program','SearchInSubdir',aOptSubFold.Checked);
cfg.WriteBool('Program','DelSuccess',aOptDelSuc.Checked);
cfg.WriteBool('Program','ClearBeforeSearch',aOptClBefSearch.Checked);
cfg.WriteBool('Program','ShowDialog',FDlgFolders.CheckBox1.Checked);
cfg.WriteBool('Program','Gridlines',aInterfGrid.Checked);
cfg.WriteBool('Program','FlatScrollbars',aInterfFlatScrb.Checked);
cfg.WriteBool('Program','RowSelect',aInterfSelRow.Checked);
cfg.WriteBool('Program','CloseSearchWin',FDlgSearch.CheckBox1.Checked);

if aFilterFF.Checked then cfg.WriteInteger('Program','Filter',0) else
  if aFilterFiles.Checked then cfg.WriteInteger('Program','Filter',1) else
    cfg.WriteInteger('Program','Filter',2);

cfg.WriteBool('Checkboxes','Checkbox001',ChBox_tmp.Checked);
cfg.WriteBool('Checkboxes','Checkbox002',ChBox_thumbs.Checked);
cfg.WriteBool('Checkboxes','Checkbox003',ChBox_bak.Checked);
cfg.WriteBool('Checkboxes','Checkbox004',ChBox_old.Checked);
cfg.WriteBool('Checkboxes','Checkbox005',ChBox_ttt.Checked);
cfg.WriteBool('Checkboxes','Checkbox006',ChBox_tttt.Checked);
cfg.WriteBool('Checkboxes','Checkbox007',ChBox_swp.Checked);
cfg.WriteBool('Checkboxes','Checkbox008',ChBox_ddd.Checked);
cfg.WriteBool('Checkboxes','Checkbox009',ChBox_dddd.Checked);
cfg.WriteBool('Checkboxes','Checkbox010',ChBox_wbk.Checked);
cfg.WriteBool('Checkboxes','Checkbox011',ChBox_bbb.Checked);
cfg.WriteBool('Checkboxes','Checkbox012',ChBox_log.Checked);
cfg.WriteBool('Checkboxes','Checkbox013',ChBox_ccc.Checked);

if cfg.SectionExists('TempFolders') then
  cfg.EraseSection('TempFolders');
for i:=0 to FDlgFolders.ListBox1.Count-1 do
  cfg.WriteString('TempFolders','Item'+IntToStr(i),FDlgFolders.ListBox1.Items[i]);

if cfg.SectionExists('CustomMasks') then
  cfg.EraseSection('CustomMasks');
if cfg.SectionExists('CustomMasksChecked') then
  cfg.EraseSection('CustomMasksChecked');
for i:=0 to FDlgCustom.CheckListBox1.Count-1 do
  begin
  cfg.WriteString('CustomMasks','Item'+IntToStr(i),FDlgCustom.CheckListBox1.Items[i]);
  cfg.WriteBool('CustomMasksChecked','Item'+IntToStr(i),FDlgCustom.CheckListBox1.Checked[i]);
  end;
cfg.UpdateFile;
end;

procedure TFMain.FormResize(Sender: TObject);
begin
if ClientHeight<386 then ClientHeight:=386;
if ClientWidth<747 then ClientWidth:=747;
ListView1.Height:=FMain.ClientHeight-30;
ListView1.Width:=FMain.ClientWidth-ListView1.Left-117;
ShellTreeView1.Height:=ListView1.Height-25;
FlatButton1.Left:=FMain.ClientWidth-112;
FlatButton2.Left:=FMain.ClientWidth-112;
GroupBox1.Left:=FMain.ClientWidth-112;
StatusBar1.Top:=FMain.ClientHeight-19;
StatusBar1.Left:=0;
StatusBar1.Width:=FMain.ClientWidth;
StatusBar1.Panels[0].Width:=FMain.ClientWidth-380;
Panel1.Height:=FMain.ClientHeight-30;
end;

procedure TFMain.FormShow(Sender: TObject);
begin
ListView1.SetFocus;
if OpeningFile<>'' then
  FMain.ReadMCXFile(OpeningFile)
else
  if OpeningDir<>''
    then ShellTreeView1.Path:=OpeningDir;
end;

// ........End Form.......


// .........Actions........

procedure TFMain.aSearchExecute(Sender: TObject);
var SomeSel: Boolean;
    i: Integer;
begin
SomeSel:=False;
for i:=0 to FDlgCustom.CheckListBox1.Count-1 do
  if FDlgCustom.CheckListBox1.Checked[i] then
    begin
    SomeSel:=True;
    Break;
    end;
if not SomeSel then ChBox_ccc.Checked:=False;

Canceled:=False;
FDlgSearch.Label1.Visible:=False;
FDlgSearch.Label6.Caption:='0';
FDlgSearch.Label7.Caption:='0';
FDlgSearch.Label8.Caption:='0 b';

FDlgSearch.Memo1.Clear;
with FDlgSearch.Memo1.Lines do
  begin
  if aFilterFF.Checked then
    Add('����� � �����')
  else
    if aFilterFiles.Checked then
      Add('������ �����')
    else
      Add('������ �����');
  if aOptSubFold.Checked then
    Add('����� �� ��������� ������');
  if ChBox_tmp.Checked then Add('*.tmp');
  if ChBox_thumbs.Checked then Add('thumbs.db');
  if ChBox_bak.Checked then Add('*.bak');
  if ChBox_old.Checked then Add('*.old');
  if ChBox_ttt.Checked then Add('~*.*');
  if ChBox_tttt.Checked then Add('*.~*');
  if ChBox_swp.Checked then Add('*.swp');
  if ChBox_ddd.Checked then Add('$*.*');
  if ChBox_dddd.Checked then Add('*.$*');
  if ChBox_wbk.Checked then Add('*.wbk');
  if ChBox_bbb.Checked then Add('backup_of_*.*');
  if ChBox_log.Checked then Add('*.log');
  if ChBox_ccc.Checked then
    for i:=0 to FDlgCustom.CheckListBox1.Count-1 do
      if FDlgCustom.CheckListBox1.Checked[i] then
        Add(FDlgCustom.CheckListBox1.Items[i]);
  end;

FDlgSearch.FlatButton1.EnableButton:=True;
FDlgSearch.FlatButton2.EnableButton:=False;
FDlgSearch.Show;
hThread:=CreateThread(nil,0,@StartFindFiles,nil,0,IDThread);

ListView1.SetFocus;
end;

procedure TFMain.aSearchTempExecute(Sender: TObject);
var SomeSel: Boolean;
    i: Integer;
begin
SomeSel:=False;
for i:=0 to FDlgCustom.CheckListBox1.Count-1 do
  if FDlgCustom.CheckListBox1.Checked[i] then
    begin
    SomeSel:=True;
    Break;
    end;
if not SomeSel then ChBox_ccc.Checked:=False;
if not FDlgFolders.CheckBox1.Checked then
  begin
  FDlgSearch.Memo1.Clear;
  with FDlgSearch.Memo1.Lines do
    begin
    Add('����� � TEMP-������');
    if aOptSubFold.Checked then
      Add('����� �� ��������� ������');
    Add('*.*');
    end;

  Canceled:=False;
  FDlgSearch.Label1.Visible:=False;
  FDlgSearch.Label6.Caption:='0';
  FDlgSearch.Label7.Caption:='0';
  FDlgSearch.Label8.Caption:='0 b';
  FDlgSearch.FlatButton1.EnableButton:=True;
  FDlgSearch.FlatButton2.EnableButton:=False;
  FDlgSearch.Show;
  hThread:=CreateThread(nil,0,@StartFindTempFiles,nil,0,IDThread);
  FMain.ListView1.SetFocus;
  end
else
  FDlgFolders.ShowModal;
end;

procedure TFMain.aStopExecute(Sender: TObject);
begin
Canceled:=True;
end;

procedure TFMain.aDelFileExecute(Sender: TObject);
var i, Atr: Integer;
    DelSizeInMB, DelSizeInKB: Real;
    Res: Boolean;
begin
for i:=ListView1.Items.Count-1 downto 0 do
  if ListView1.Items[i].Selected then
    begin
    if ListView1.Items[i].SubItems[0]<>'[ dir ]' then
      begin
      Atr:=FileGetAttr(ListView1.Items[i].Caption);
      if Atr and faReadOnly <> 0 then
        FileSetAttr(ListView1.Items[i].Caption, Atr - faReadOnly);

      if not aOptUseRec.Checked then
        Res:=DeleteFile(ListView1.Items[i].Caption)
      else
        if ListView1.Items[i].SubItems[1]<>'������' then
          Res:=RecycleFile(ListView1.Items[i].Caption);

      if Res then
        begin
        ListView1.Items[i].SubItems[1]:='������';
        Inc(DeletedFiles);
        DeletedSize:=DeletedSize+StrToInt(ListView1.Items[i].SubItems[2]);
        if aOptDelSuc.Checked then DelItemFromList(i);
        end
      else
        if ListView1.Items[i].SubItems[1]<>'������' then
          ListView1.Items[i].SubItems[1]:='������';
      end
    else
      begin
      Atr:=FileGetAttr(ListView1.Items[i].Caption);
      if Atr and faReadOnly <> 0
        then FileSetAttr(ListView1.Items[i].Caption, Atr - faReadOnly );
      if RemoveDir(ListView1.Items[i].Caption) then
        begin
        ListView1.Items[i].SubItems[1]:='������';
        Inc(DeletedDirs);
        if aOptDelSuc.Checked then DelItemFromList(i);
        end
      else
        if ListView1.Items[i].SubItems[1] <> '������'
          then ListView1.Items[i].SubItems[1]:='������';
      end;
    end;

if aOptDelSuc.Checked then
begin
  CheckTotalSize;
  CheckSelected;
end;

DelSizeInKB:=round(DeletedSize/102.4)/10;
if DelSizeInKB > 1 then
  begin
  DelSizeInMB:=round(DeletedSize/10485.76)/100;
  if DelSizeInMB > 0.9 then
    SDeletedSize:=FloatToStr(DelSizeInMB)+' M'
  else
    SDeletedSize:=FloatToStr(DelSizeInKB)+' k';
  end
else
  SDeletedSize:=IntToStr(DeletedSize)+' b';
StatusBar1.Panels[6].Text:=IntToStr(DeletedFiles)+'/'+IntToStr(DeletedDirs)+'/'+SDeletedSize;
end;

procedure TFMain.aDelFilesAllExecute(Sender: TObject);
var i, n, Atr: Integer;
    DelSizeInMB, DelSizeInKB: Real;
    Res: Boolean;
begin
for i:=ListView1.Items.Count-1 downto 0 do
  begin
  if ListView1.Items[i].SubItems[0]<>'[ dir ]' then
    begin
    Atr:=FileGetAttr(ListView1.Items[i].Caption);
    if Atr and faReadOnly <> 0 then
      FileSetAttr(ListView1.Items[i].Caption, Atr - faReadOnly );

    if not aOptUseRec.Checked then
      Res:=DeleteFile(ListView1.Items[i].Caption)
    else
      if ListView1.Items[i].SubItems[1]<>'������' then
        Res:=RecycleFile(ListView1.Items[i].Caption);

    if Res then
      begin
      ListView1.Items[i].SubItems[1]:='������';
      Inc(DeletedFiles);
      DeletedSize:=DeletedSize+StrToInt(ListView1.Items[i].SubItems[2]);
      if aOptDelSuc.Checked then DelItemFromList(i);
      end
    else
      if ListView1.Items[i].SubItems[1]<>'������' then
        ListView1.Items[i].SubItems[1]:='������';
    end;
  end;
i:=0;
n:=ListView1.Items.Count-1;
while i<=n do
  begin
  if ListView1.Items[i].SubItems[0]='[ dir ]' then
    begin
    if RemoveDir(ListView1.Items[i].Caption) then
      begin
      ListView1.Items[i].SubItems[1]:='������';
      Inc(DeletedDirs);
      if aOptDelSuc.Checked then DelItemFromList(i);
      end
    else
      if ListView1.Items[i].SubItems[1]<>'������'
        then ListView1.Items[i].SubItems[1]:='������';
    end;
  Inc(i);
  n:=ListView1.Items.Count-1;
  end;

if aOptDelSuc.Checked then CheckTotalSize;

DelSizeInKB:=round(DeletedSize/102.4)/10;
if DelSizeInKB > 1 then
  begin
  DelSizeInMB:=round(DeletedSize/10485.76)/100;
  if DelSizeInMB > 0.9 then
    SDeletedSize:=FloatToStr(DelSizeInMB)+' M'
  else
    SDeletedSize:=FloatToStr(DelSizeInKB)+' k';
  end
else
  SDeletedSize:=IntToStr(DeletedSize)+' b';
StatusBar1.Panels[6].Text:=IntToStr(DeletedFiles)+'/'+IntToStr(DeletedDirs)+'/'+SDeletedSize;
end;

procedure TFMain.aClearRecExecute(Sender: TObject);
begin
EmptyRecycleBin;
end;

procedure TFMain.aExitExecute(Sender: TObject);
begin
Close;
end;
//---------------------------------------------------
procedure TFMain.aListOpenExecute(Sender: TObject);
begin
if OpenDialog1.Execute then
  try
    ReadMCXFile(OpenDialog1.FileName);
  except end;
end;

procedure TFMain.aListSaveExecute(Sender: TObject);
begin
if SaveDialog1.Execute then
  WriteMCXFile(SaveDialog1.FileName);
end;

procedure TFMain.aDelItemExecute(Sender: TObject);
var i: Integer;
begin
if ListView1.Focused then
  for i:=ListView1.Items.Count-1 downto 0 do
    if ListView1.Items[i].Selected then
      DelItemFromList(i);
CheckTotalSize;
CheckSelected;
end;

procedure TFMain.aDelItemsAllExecute(Sender: TObject);
begin
ListView1.Clear;
TotalFiles:=0;
TotalDirs:=0;
TotalSize:=0;
STotalSize:=IntToStr(TotalSize)+' b';
StatusBar1.Panels[2].Text:=IntToStr(TotalFiles)+'/'+IntToStr(TotalDirs)+'/'+STotalSize;
CheckSelected;
end;
//-------------------------------------------------
procedure TFMain.aSelAllExecute(Sender: TObject);
var i: Integer;
begin
for i:=0 to ListView1.Items.Count-1 do
  begin
  ListView1.Items[i].Checked:=True;
  ListView1.Items[i].Selected:=True;
  end;
ListView1.SetFocus;
CheckSelected;
end;

procedure TFMain.aSelNoneExecute(Sender: TObject);
var i: Integer;
begin
for i:=0 to ListView1.Items.Count-1 do
  begin
  ListView1.Items[i].Checked:=False;
  ListView1.Items[i].Selected:=False;
  end;
ListView1.SetFocus;
CheckSelected;
end;

procedure TFMain.aSelInvExecute(Sender: TObject);
var i: Integer;
begin
for i:=0 to ListView1.Items.Count-1 do
  begin
  ListView1.Items[i].Checked:=not ListView1.Items[i].Checked;
  ListView1.Items[i].Selected:=ListView1.Items[i].Checked;
  end;
ListView1.SetFocus;
CheckSelected;
end;
//-------------------------------------------------
procedure TFMain.aShowFDlgAssocExecute(Sender: TObject);
begin
FDlgAssoc.ShowModal;
end;

procedure TFMain.aShowFDlgFolderExecute(Sender: TObject);
begin
FDlgFolders.FlatButton2.EnableButton:=False;
FDlgFolders.ShowModal;
end;
//-------------------------------------------------
procedure TFMain.aInterfGridExecute(Sender: TObject);
begin
ListView1.GridLines:=aInterfGrid.Checked;
end;

procedure TFMain.aInterfFlatScrbExecute(Sender: TObject);
begin
ListView1.FlatScrollBars:=aInterfFlatScrb.Checked;
end;

procedure TFMain.aInterfSelRowExecute(Sender: TObject);
begin
ListView1.RowSelect:=aInterfSelRow.Checked;
ListView1.Refresh;
end;

procedure TFMain.aInterfSizingExecute(Sender: TObject);
begin
WindowState:=wsNormal;
ClientWidth:=747;
ClientHeight:=386;
ListView1.Left:=176;
ListView1.Width:=454;
Panel1.Left:=171;
Edit1.Width:=166;
ShellTreeView1.Width:=166;
end;
//-------------------------------------------------
procedure TFMain.aOptUseRecExecute(Sender: TObject);
begin
{ Empty }
end;

procedure TFMain.aOptSubFoldExecute(Sender: TObject);
begin
{ Empty }
end;

procedure TFMain.aOptDelSucExecute(Sender: TObject);
var i: Integer;
begin
if n10.Checked then
  for i:=ListView1.Items.Count-1 downto 0 do
    if ListView1.Items[i].SubItems[1]='������' then
      DelItemFromList(i);
CheckTotalSize;
end;

procedure TFMain.aOptClBefSearchExecute(Sender: TObject);
begin
{ Empty }
end;

procedure TFMain.aOptResetExecute(Sender: TObject);
begin
DeletedFiles:=0;
DeletedDirs:=0;
DeletedSize:=0;
SDeletedSize:=IntToStr(DeletedSize)+' b';
StatusBar1.Panels[6].Text:=IntToStr(DeletedFiles)+'/'+IntToStr(DeletedDirs)+'/'+SDeletedSize;
end;
//-------------------------------------------------

procedure TFMain.aFilterFFExecute(Sender: TObject);
begin
aFilterFF.Checked:=True;
aFilterFiles.Checked:=False;
aFilterFold.Checked:=False;
end;

procedure TFMain.aFilterFilesExecute(Sender: TObject);
begin
aFilterFF.Checked:=False;
aFilterFiles.Checked:=True;
aFilterFold.Checked:=False;
end;

procedure TFMain.aFilterFoldExecute(Sender: TObject);
begin
aFilterFF.Checked:=False;
aFilterFiles.Checked:=False;
aFilterFold.Checked:=True;
end;
//-------------------------------------------------
procedure TFMain.aCheckboxesAllExecute(Sender: TObject);
begin
ChBox_tmp.Checked:=True;
ChBox_thumbs.Checked:=True;
ChBox_bak.Checked:=True;
ChBox_old.Checked:=True;
ChBox_ttt.Checked:=True;
ChBox_tttt.Checked:=True;
ChBox_swp.Checked:=True;
ChBox_ddd.Checked:=True;
ChBox_dddd.Checked:=True;
ChBox_wbk.Checked:=True;
ChBox_bbb.Checked:=True;
ChBox_log.Checked:=True;
ChBox_ccc.Checked:=True;
end;

procedure TFMain.aCheckboxesNoneExecute(Sender: TObject);
begin
ChBox_tmp.Checked:=False;
ChBox_thumbs.Checked:=False;
ChBox_bak.Checked:=False;
ChBox_old.Checked:=False;
ChBox_ttt.Checked:=False;
ChBox_tttt.Checked:=False;
ChBox_swp.Checked:=False;
ChBox_ddd.Checked:=False;
ChBox_dddd.Checked:=False;
ChBox_wbk.Checked:=False;
ChBox_bbb.Checked:=False;
ChBox_log.Checked:=False;
ChBox_ccc.Checked:=False;
end;
//-------------------------------------------------
procedure TFMain.aShowReadMetxtExecute(Sender: TObject);
var SF: String;
begin
SF:=ExtractFilePath(ParamStr(0))+'ReadMe.txt';
if FileExists(SF) then
  ShellExecute(0,'',PChar(SF),'','',1)
else
  Application.MessageBox(pchar('������ ��� ������ ����� ��� ���� �� ������'+#13+SF),PChar(FMain.Caption),MB_ICONERROR+mb_OK);
end;

procedure TFMain.aShowFDlgAboutExecute(Sender: TObject);
begin
FDlgAbout.ShowModal;
end;
//-------------------------------------------------
procedure TFMain.aFileOpenExecute(Sender: TObject);
begin
try
  ShellExecute(0,'',PChar(ListView1.Items[RightButtonItem].Caption),'','',1);
  except end;
end;

procedure TFMain.aFileInFolderExecute(Sender: TObject);
begin
try
  ShellExecute(0,'',PChar(ExtractFileDir(ListView1.Items[RightButtonItem].Caption)),'','',1);
  except end;
end;

procedure TFMain.aFilePropExecute(Sender: TObject);
var
  SF: String;
begin
  if (ListView1.Items.Count <> 0) then
  begin
    SF:=ListView1.Items[RightButtonItem].Caption;
    try
      if (FileExists(SF))or(DirectoryExists(SF)) then
        ShowPropertiesDialog(SF);
    except
    end;
  end;
end;

// .........End Actions........

procedure TFMain.Edit1Change(Sender: TObject);
begin
Edit1.SelStart:=Length(Edit1.Text);
end;

procedure TFMain.ShellTreeView1Change(Sender: TObject; Node: TTreeNode);
var c: Char;
begin
Edit1.Text:=ShellTreeView1.Path;
if (FMain.ShellTreeView1.Path='��� ���������')or(FMain.ShellTreeView1.Path='My computer') then
  begin
  Edit1.Text:=Edit1.Text+' (';
  for c:='A' to 'Z' do
    if GetDriveType(PChar(c+':\'))=DRIVE_FIXED
      then Edit1.Text:=Edit1.Text + c+': ';
  Edit1.Text:=Edit1.Text+')';
  end;
end;

procedure TFMain.Edit1KeyPress(Sender: TObject; var Key: Char);
begin
if Key=#13 then
  begin
  try
    ShellTreeView1.Path:=Edit1.Text;
  except end;
  Key:=#0;
  end;
end;

procedure TFMain.ListView1DblClick(Sender: TObject);
begin
aFileProp.Execute;
end;

procedure TFMain.FlatButton3Click(Sender: TObject);
begin
FDlgCustom.ShowModal;
ChBox_ccc.Checked:=True;
end;

procedure TFMain.ListView1SelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
Item.Selected:=Item.Checked;
end;

procedure TFMain.ListView1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var Item: TListItem;
    i: Integer;
begin
try
  Item:=ListView1.GetItemAt(X,Y);
  RightButtonItem:=Item.Index;
  if Shift<>[ssRight] then
    begin
    IndOld:=IndNew;
    Item.Selected:=not Item.Selected;
    Item.Checked:=not Item.Checked;
    IndNew:=Item.Index;
    end;
except end;

if Shift=[ssLeft,ssShift] then
    begin
    if IndOld<IndNew then
      begin
      for i:=IndOld to IndNew do
        begin
        ListView1.Items[i].Checked:=ListView1.Items[IndOld].Checked;
        end;
      end
    else
      begin
      for i:=IndNew to IndOld do
        begin
        ListView1.Items[i].Checked:=ListView1.Items[IndOld].Checked;
        end;
      end;
    end;

for i:=0 to ListView1.Items.Count-1 do
  ListView1.Items[i].Selected:=ListView1.Items[i].Checked;
end;

procedure TFMain.ListView1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var i: Integer;
begin
for i:=0 to ListView1.Items.Count-1 do
  ListView1.Items[i].Selected:=ListView1.Items[i].Checked;

CheckSelected;
end;

procedure TFMain.FlatButton1Click(Sender: TObject);
begin
aSearch.Execute
end;

procedure TFMain.Panel1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if Shift=[ssLeft] then
  Moving:=True;
end;

procedure TFMain.Panel1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var P: TPoint;
begin
StatusBar1.Panels[0].Text:='';
if Moving then
  begin
  GetCursorPos(P);
  P:=ScreenToClient(P);
  Panel1.Left:=P.X;
  ShellTreeView1.Width:=Panel1.Left - ShellTreeView1.Left;
  Edit1.Width:=Panel1.Left - Edit1.Left;
  ListView1.Left:=Panel1.Left + Panel1.Width;
  ListView1.Width:=FMain.ClientWidth-ListView1.Left-117;
  end;
end;

procedure TFMain.Panel1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
Moving:=False;
end;

procedure TFMain.ListView1Resize(Sender: TObject);
begin
ListView1.Columns[0].Width:=ListView1.Width-132;
ListView1.Columns[1].Width:=58;
ListView1.Columns[2].Width:=53;
end;

procedure TFMain.ListView1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var Item: TListItem;
    i: Integer;
begin
try
  Item:=ListView1.GetItemAt(X,Y);
  StatusBar1.Panels[0].Text:=MinimizeName(Item.Caption,StatusBar1.Canvas,StatusBar1.Panels[0].Width);
except
  StatusBar1.Panels[0].Text:='';
  end;
end;

procedure TFMain.StatusBar1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
if Shift=[ssRight] then
Application.MessageBox(PChar(
  '�������: '+STotalSize+' ('+IntToStr(TotalSize)+' b); '+'������: '+IntToStr(TotalFiles)+'; �����: '+IntToStr(TotalDirs)+#13+
  '��������: '+SSelectedSize+' ('+IntToStr(SelectedSize)+' b); '+'������: '+IntToStr(SelectedFiles)+'; �����: '+IntToStr(SelectedDirs)+#13+
  '�������: '+SDeletedSize+' ('+IntToStr(DeletedSize)+' b); '+'������: '+IntToStr(DeletedFiles)+'; �����: '+IntToStr(DeletedDirs)+#13
  ),PChar(FMain.Caption),MB_OK+MB_ICONINFORMATION);
end;

procedure TFMain.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
StatusBar1.Panels[0].Text:='';
end;

procedure TFMain.StatusBar1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
StatusBar1.Panels[0].Text:='<������> / <�����> / <������>';
end;

end.
