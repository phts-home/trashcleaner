unit uView;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls;

type
  TFView = class(TForm)
    Memo1: TMemo;
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FView: TFView;

implementation

uses uMain;

{$R *.dfm}

procedure TFView.FormResize(Sender: TObject);
begin
Memo1.Width:=ClientWidth-20;
Memo1.Height:=ClientHeight-19;
end;

procedure TFView.FormShow(Sender: TObject);
begin
Left:=FMain.Left+151;
Top:=FMain.Top+53;
end;

end.
