unit uAbout;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ShellApi, ExtCtrls, FlatButton;

type
  TFDlgAbout = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    FlatButton1: TFlatButton;
    Image1: TImage;
    Label9: TLabel;
    procedure Label6Click(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure FlatButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgAbout: TFDlgAbout;

implementation

uses uMain;

{$R *.dfm}

procedure TFDlgAbout.Label6Click(Sender: TObject);
begin
ShellExecute(0,'',pchar('mailto:'+Label6.Caption+'?subject=TrashCleaner'),'','',1);
end;

procedure TFDlgAbout.Label7Click(Sender: TObject);
begin
ShellExecute(0,'',pchar(Label7.Caption),'','',1);
end;

procedure TFDlgAbout.FlatButton1Click(Sender: TObject);
begin
Close;
end;

procedure TFDlgAbout.FormShow(Sender: TObject);
begin
Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;

end.
