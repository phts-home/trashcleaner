unit uFolders;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, FlatButton, ComCtrls, ShellCtrls;

type
  TFDlgFolders = class(TForm)
    ShellTreeView1: TShellTreeView;
    ListBox1: TListBox;
    FlatButton1: TFlatButton;
    FlatButton2: TFlatButton;
    CheckBox1: TCheckBox;
    FlatButton3: TFlatButton;
    FlatButton4: TFlatButton;
    Label1: TLabel;
    FlatButton5: TFlatButton;
    procedure FlatButton1Click(Sender: TObject);
    procedure FlatButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FlatButton3Click(Sender: TObject);
    procedure FlatButton4Click(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure FlatButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    procedure CheckHorizontalBar (ListBox: TListBox);
    procedure GetEnvironmentStrings(ss:TStrings);
    { Public declarations }
  end;

var
  FDlgFolders: TFDlgFolders;

implementation

uses uMain;

{$R *.dfm}

procedure StartFindTempFiles;
var i:integer;
begin
FMain.FlatButton1.EnableButton:=False;
FMain.FlatButton2.EnableButton:=False;
FMain.aDelFilesAll.Enabled:=False;
FMain.aDelFile.Enabled:=False;
FMain.aDelItem.Enabled:=False;
FMain.aDelItemsAll.Enabled:=False;
FMain.aSearch.Enabled:=False;
FMain.ListView1.PopupMenu:=nil;
if FMain.aOptClBefSearch.Checked then
  begin
  FMain.aDelItemsAll.Execute;
  end;
for i:=0 to FDlgFolders.ListBox1.Items.Count-1 do
  begin
  FMain.FindFiles(FDlgFolders.ListBox1.Items[i],FMain.ListView1,true,true);
  end;

FMain.FlatButton1.EnableButton:=True;
FMain.FlatButton2.EnableButton:=True;
FMain.aDelFilesAll.Enabled:=True;
FMain.aDelFile.Enabled:=True;
FMain.aDelItem.Enabled:=True;
FMain.aDelItemsAll.Enabled:=True;
FMain.aSearch.Enabled:=True;
FMain.ListView1.PopupMenu:=FMain.PopupMenu1;
end;

procedure TFDlgFolders.CheckHorizontalBar (ListBox: TListBox);
var i, max: Integer;
begin
max:=0;
for i:=0 to ListBox.items.count-1 do
  begin
  if length(ListBox.items[i])>max
    then max:=length(ListBox.items[i]);
  end;
SendMessage(ListBox.Handle,lb_sethorizontalextent,max*9,0);
end;

procedure TFDlgFolders.GetEnvironmentStrings(ss:TStrings);
var ptr: PChar;
    s: string;
    Done: boolean;
begin
ss.Clear;
s:='';
Done:=False;
ptr:=Windows.GetEnvironmentStrings;
while Done=False do begin
  if ptr^=#0 then
    begin
    Inc(ptr);
    if ptr^=#0
      then Done:=True
      else ss.Add(s);
    s:=ptr^;
    end
  else s:=s+ptr^;
  Inc(ptr);
  end;
end;


procedure TFDlgFolders.FormCreate(Sender: TObject);
var i: Integer;
    Strings: TStringList;
begin
CheckBox1.Checked:=FMain.cfg.ReadBool('Program','ShowDialog',True);
if FMain.cfg.SectionExists('TempFolders') then
  begin
  Strings:=TStringList.Create;
  FMain.cfg.ReadSection('TempFolders',Strings);
  for i:=0 to Strings.Count-1 do
    ListBox1.Items.Add(FMain.cfg.ReadString('TempFolders','Item'+IntToStr(i),''));
  Strings.Free;
  end;

if ListBox1.Count = 0 then
  begin
  Strings:=TStringList.Create;
  GetEnvironmentStrings(Strings);
  for i:=0 to Strings.Count-1 do
    if Copy(Strings[i],0,4) = 'TEMP' then
      ListBox1.Items.Add(Copy(Strings[i],6,Length(Strings[i])-5));
  Strings.Free;
  if DirectoryExists('c:\Windows\Temp') then
    ListBox1.Items.Add('c:\Windows\Temp');
  end;

CheckHorizontalBar(ListBox1);
end;

procedure TFDlgFolders.FormClose(Sender: TObject; var Action: TCloseAction);
begin
FDlgFolders.FlatButton2.EnableButton:=True;
end;

procedure TFDlgFolders.FormShow(Sender: TObject);
begin
Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;



procedure TFDlgFolders.FlatButton1Click(Sender: TObject);
var i: Integer;
    PathEx: Boolean;
begin
PathEx:=False;
for i:=0 to ListBox1.Items.Count-1 do
  begin
  if ShellTreeView1.Path=ListBox1.Items[i] then
    begin
    PathEx:=true;
    Break;
    end;
  end;
if not PathEx then
  ListBox1.Items.Add(ShellTreeView1.Path);
CheckHorizontalBar(ListBox1);
end;

procedure TFDlgFolders.FlatButton2Click(Sender: TObject);
var IDThread: Cardinal;
begin
CreateThread(nil,0,@StartFindTempFiles,nil,0,IDThread);
FMain.ListView1.SetFocus;
Close;
end;

procedure TFDlgFolders.FlatButton3Click(Sender: TObject);
begin
ListBox1.Items.Delete(ListBox1.ItemIndex);
CheckHorizontalBar(ListBox1);
end;

procedure TFDlgFolders.FlatButton4Click(Sender: TObject);
begin
Close;
end;

procedure TFDlgFolders.FlatButton5Click(Sender: TObject);
begin
Application.MessageBox(pchar('��������� �����:'+#13+'X:\Documents and Settings\<���� ��. ������>\Local Settings\Temp\'
      +#13+'X:\Windows\Temp\'+#13#13+'X - ������, �� ������� ����������� Windows XP'+#13+'�� ������ �������� �� ������ ��� �����, �� � ����� ������'),pchar(FMain.Caption),MB_OK+MB_ICONINFORMATION);
end;

procedure TFDlgFolders.ListBox1DblClick(Sender: TObject);
begin
try
  ShellTreeView1.Path:=ListBox1.Items[ListBox1.ItemIndex];
  except end;
end;

procedure TFDlgFolders.ListBox1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
if Key = 46 then
  FlatButton3Click(FlatButton3);
end;

end.
