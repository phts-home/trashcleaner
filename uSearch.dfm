object FDlgSearch: TFDlgSearch
  Left = 185
  Top = 230
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1055#1086#1080#1089#1082
  ClientHeight = 214
  ClientWidth = 469
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 5
    Top = 6
    Width = 456
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'Panel1'
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    object Gauge1: TGauge
      Left = 5
      Top = 4
      Width = 444
      Height = 15
      BackColor = clBtnFace
      BorderStyle = bsNone
      ForeColor = 12348252
      Progress = 100
      ShowText = False
    end
    object Label1: TLabel
      Left = 200
      Top = 5
      Width = 85
      Height = 13
      Caption = #1055#1086#1080#1089#1082' '#1079#1072#1074#1077#1088#1096#1077#1085
      Color = 12348252
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clAqua
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      Transparent = True
      Visible = False
    end
  end
  object FlatButton1: TFlatButton
    Left = 245
    Top = 181
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    OnClick = FlatButton1Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clNavy
    FontColorHighLight = clNavy
    FontColorPushedState = clNavy
    FontColorDisabled = clGray
  end
  object FlatButton2: TFlatButton
    Left = 355
    Top = 181
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1047#1072#1082#1088#1099#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    OnClick = FlatButton2Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clBlack
    FontColorHighLight = clBlack
    FontColorPushedState = clBlack
    FontColorDisabled = clGray
  end
  object GroupBox1: TGroupBox
    Left = 5
    Top = 35
    Width = 456
    Height = 46
    Caption = ' '#1058#1077#1082#1091#1097#1072#1103' '#1087#1072#1087#1082#1072'  '
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    object Label4: TLabel
      Left = 5
      Top = 19
      Width = 444
      Height = 13
      AutoSize = False
      Caption = 'Label4'
    end
  end
  object GroupBox2: TGroupBox
    Left = 245
    Top = 85
    Width = 216
    Height = 73
    Caption = ' '#1053#1072#1081#1076#1077#1085#1086'  '
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 4
    object Label2: TLabel
      Left = 5
      Top = 18
      Width = 44
      Height = 13
      Caption = #1060#1072#1081#1083#1086#1074':'
    end
    object Label3: TLabel
      Left = 5
      Top = 33
      Width = 35
      Height = 13
      Caption = #1055#1072#1087#1086#1082':'
    end
    object Label5: TLabel
      Left = 5
      Top = 48
      Width = 79
      Height = 13
      Caption = #1054#1073#1097#1080#1081' '#1088#1072#1079#1084#1077#1088':'
    end
    object Label6: TLabel
      Left = 100
      Top = 18
      Width = 6
      Height = 13
      Caption = '0'
    end
    object Label7: TLabel
      Left = 100
      Top = 33
      Width = 6
      Height = 13
      Caption = '0'
    end
    object Label8: TLabel
      Left = 100
      Top = 48
      Width = 6
      Height = 13
      Caption = '0'
    end
  end
  object GroupBox3: TGroupBox
    Left = 5
    Top = 85
    Width = 236
    Height = 96
    Caption = ' '#1054#1087#1094#1080#1080'  '
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 5
    object Memo1: TMemo
      Left = 5
      Top = 20
      Width = 226
      Height = 66
      BorderStyle = bsNone
      Color = clBtnFace
      Lines.Strings = (
        'Memo1')
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object Panel2: TPanel
    Left = 5
    Top = 180
    Width = 236
    Height = 26
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 6
    object CheckBox1: TCheckBox
      Left = 5
      Top = 4
      Width = 224
      Height = 17
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1087#1086' '#1079#1072#1074#1077#1088#1096#1077#1085#1080#1102
      TabOrder = 0
    end
  end
end
