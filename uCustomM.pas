unit uCustomM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, FlatButton, CheckLst;

type
  TFDlgCustom = class(TForm)
    CheckListBox1: TCheckListBox;
    FlatButton1: TFlatButton;
    FlatButton3: TFlatButton;
    Edit1: TEdit;
    FlatButton5: TFlatButton;
    procedure FlatButton5Click(Sender: TObject);
    procedure FlatButton1Click(Sender: TObject);
    procedure FlatButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CheckListBox1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgCustom: TFDlgCustom;

implementation

uses uMain;

{$R *.dfm}

procedure TFDlgCustom.FlatButton5Click(Sender: TObject);
begin
Close;
end;

procedure TFDlgCustom.FlatButton1Click(Sender: TObject);
var i: Integer;
    MaskEx: Boolean;
begin
if Edit1.Text<>'' then
  begin
  MaskEx:=False;
  for i:=0 to CheckListBox1.Count-1 do
    if CheckListBox1.Items[i] = Edit1.Text
      then begin MaskEx:=True; Break; end;
  if not MaskEx then
    begin
    CheckListBox1.Items.Add(Edit1.Text);
    CheckListBox1.Checked[CheckListBox1.Count-1]:=True;
    end;
  Edit1.SelectAll;
  end;
end;

procedure TFDlgCustom.FlatButton3Click(Sender: TObject);
begin
CheckListBox1.DeleteSelected;
end;

procedure TFDlgCustom.FormCreate(Sender: TObject);
var i: Integer;
    Strings: TStringList;
begin
if FMain.cfg.SectionExists('CustomMasks') then
  begin
  Strings:=TStringList.Create;
  FMain.cfg.ReadSection('CustomMasks',Strings);
  for i:=0 to Strings.Count-1 do
    begin
    CheckListBox1.Items.Add(FMain.cfg.ReadString('CustomMasks','Item'+IntToStr(i),''));
    CheckListBox1.Checked[i]:=FMain.cfg.ReadBool('CustomMasksChecked','Item'+IntToStr(i),False);
    end;
  Strings.Free;
  end;
end;

procedure TFDlgCustom.FormKeyPress(Sender: TObject; var Key: Char);
begin
case Key of
  #13:
    begin
    FlatButton1Click(FlatButton1);
    Key:=#0;
    end;
  #27: Close;
  end;
end;

procedure TFDlgCustom.CheckListBox1Click(Sender: TObject);
begin
Edit1.Text:=CheckListBox1.Items[CheckListBox1.ItemIndex];
Edit1.SetFocus;
Edit1.SelectAll;
end;

procedure TFDlgCustom.FormClose(Sender: TObject;
  var Action: TCloseAction);
var i: Integer;
    SomeChecked: Boolean;
begin
SomeChecked:=False;
for i:=0 to CheckListBox1.Count-1 do
  if CheckListBox1.Checked[i] then
    begin
    SomeChecked:=True;
    Break;
    end;
if not SomeChecked then FMain.ChBox_ccc.Checked:=False;
end;

procedure TFDlgCustom.FormShow(Sender: TObject);
begin
Left:=FMain.Left + FMain.Width - 340;
Top:=FMain.Top + 250;
end;

end.
