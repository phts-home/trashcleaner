program Cleaner;

uses
  Forms,
  uMain in 'uMain.pas' {FMain},
  uAbout in 'uAbout.pas' {FDlgAbout},
  uView in 'uView.pas' {FView},
  uFolders in 'uFolders.pas' {FDlgFolders},
  uAssociation in 'uAssociation.pas' {FDlgAssoc},
  uCustomM in 'uCustomM.pas' {FDlgCustom},
  uSearch in 'uSearch.pas' {FDlgSearch};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'TrashCleaner';
  Application.CreateForm(TFMain, FMain);
  Application.CreateForm(TFDlgAbout, FDlgAbout);
  Application.CreateForm(TFDlgFolders, FDlgFolders);
  Application.CreateForm(TFDlgAssoc, FDlgAssoc);
  Application.CreateForm(TFDlgCustom, FDlgCustom);
  Application.CreateForm(TFDlgSearch, FDlgSearch);
  FMain.OpeningFile:='';
FMain.OpeningDir:='';
if ParamCount>0 then
  begin
  if ParamStr(1)='/dir' then
    begin
    FMain.OpeningDir:=ParamStr(2);
    end else
    begin
    FMain.OpeningFile:=ParamStr(1);
    try
      FMain.ShellTreeView1.Path:=FMain.cfg.ReadString('program','path','C:\');
      except end;
    end;
  end else
  begin
  try
    FMain.ShellTreeView1.Path:=FMain.cfg.ReadString('program','path','C:\');
    except end;
  end;
  Application.Run;
end.
