unit uAssociation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, FlatButton, StdCtrls, Registry;

type
  TFDlgAssoc = class(TForm)
    Label1: TLabel;
    Shape1: TShape;
    Label2: TLabel;
    Shape2: TShape;
    FlatButton1: TFlatButton;
    FlatButton2: TFlatButton;
    FlatButton3: TFlatButton;
    FlatButton4: TFlatButton;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    FlatButton5: TFlatButton;
    Edit1: TEdit;
    procedure FlatButton1Click(Sender: TObject);
    procedure FlatButton2Click(Sender: TObject);
    procedure FlatButton3Click(Sender: TObject);
    procedure FlatButton4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FlatButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure AssociateMCX;
    procedure AssociateDir;
    procedure DeassociateMCX;
    procedure DeassociateDir;
    procedure CheckAssociation;
    { Public declarations }
  end;

var
  FDlgAssoc: TFDlgAssoc;

implementation

uses uMain;

{$R *.dfm}


procedure TFDlgAssoc.AssociateDir;
var Reg: TRegistry;
begin
try
  Reg:=TRegistry.Create;
  Reg.RootKey:=HKEY_CLASSES_ROOT;
  finally
    Reg.OpenKey('Directory\shell\Open in TrashCleaner', True);
    Reg.WriteString('',Edit1.Text);
    Reg.CloseKey;
    Reg.OpenKey('Directory\shell\Open in TrashCleaner\command', True);
    Reg.WriteString('',Application.ExeName+' /dir "%1"');
    Reg.CloseKey;
    Reg.OpenKey('Drive\shell\Open in TrashCleaner', True);
    Reg.WriteString('',Edit1.Text);
    Reg.CloseKey;
    Reg.OpenKey('Drive\shell\Open in TrashCleaner\command', True);
    Reg.WriteString('',Application.ExeName+' /dir "%1"');
    Reg.CloseKey;
    Reg.Free;
  end;
end;

procedure TFDlgAssoc.AssociateMCX;
var Reg: TRegistry;
begin
try
  Reg:=TRegistry.Create;
  Reg.RootKey:=HKEY_CLASSES_ROOT;
finally
  Reg.OpenKey('.mcx', True);
  Reg.WriteString('', 'mcxfile');
  Reg.CloseKey;
  Reg.OpenKey('mcxfile\DefaultIcon', True);
  Reg.WriteString('', ExtractFilePath(paramstr(0))+'FileIcon.ico');
  Reg.CloseKey;
  Reg.OpenKey('mcxfile\shell\open\command', True);
  Reg.WriteString('', Application.ExeName+' "%1"');
  Reg.CloseKey;
  Reg.OpenKey('mcxfile\', True);
  Reg.WriteString('', FMain.Caption+' List');
  Reg.CloseKey;
  Reg.Free;
  end;
end;

procedure TFDlgAssoc.DeassociateDir;
var Reg: TRegistry;
begin
try
  Reg:=TRegistry.Create;
  Reg.RootKey:=HKEY_CLASSES_ROOT;
finally
  Reg.DeleteKey('Directory\shell\Open in TrashCleaner');
  Reg.DeleteKey('Drive\shell\Open in TrashCleaner');
  Reg.Free;
  end;
end;

procedure TFDlgAssoc.DeassociateMCX;
var Reg: TRegistry;
begin
try
  Reg:=TRegistry.Create;
  Reg.RootKey:=HKEY_CLASSES_ROOT;
finally
  Reg.DeleteKey('.mcx');
  Reg.DeleteKey('mcxfile\');
  Reg.Free;
  end;
end;

procedure TFDlgAssoc.CheckAssociation;
var Reg: TRegistry;
begin
try
  Reg:=TRegistry.Create;
  Reg.RootKey:=HKEY_CLASSES_ROOT;
finally
  Label6.Caption:='���';
  if Reg.KeyExists('Directory\shell')then
    if Reg.KeyExists('Directory\shell\Open in TrashCleaner')then
      begin
      Reg.OpenKey('Directory\shell\Open in TrashCleaner',True);
      Edit1.Text:=Reg.ReadString('');
      Reg.CloseKey;
      if Reg.KeyExists('Directory\shell\Open in TrashCleaner\command')then
        begin
        Reg.OpenKey('Directory\shell\Open in TrashCleaner\command',True);
        if Reg.ReadString('') = Application.ExeName+' /dir "%1"' then
          Label6.Caption:='����';
        Reg.CloseKey;
        end;
      end;
  Label5.Caption:='���';
  if Reg.KeyExists('mcxfile')then
    if Reg.KeyExists('mcxfile\shell')then
      if Reg.KeyExists('mcxfile\shell\open')then
        if Reg.KeyExists('mcxfile\shell\open\command')then
          begin
          Reg.OpenKey('mcxfile\shell\open\command', True);
          if Reg.ReadString('') = Application.ExeName+' "%1"' then
            Label5.Caption:='����';
          Reg.CloseKey;
          end;
  Reg.Free;
  end;
end;



procedure TFDlgAssoc.FormShow(Sender: TObject);
var Reg: TRegistry;
begin
Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
CheckAssociation;
end;


procedure TFDlgAssoc.FlatButton1Click(Sender: TObject);
begin
AssociateMCX;
CheckAssociation;
end;

procedure TFDlgAssoc.FlatButton2Click(Sender: TObject);
begin
DeassociateMCX;
CheckAssociation;
end;

procedure TFDlgAssoc.FlatButton3Click(Sender: TObject);
begin
AssociateDir;
CheckAssociation;
end;

procedure TFDlgAssoc.FlatButton4Click(Sender: TObject);
begin
DeassociateDir;
CheckAssociation;
end;

procedure TFDlgAssoc.FlatButton5Click(Sender: TObject);
begin
Close;
end;



end.
