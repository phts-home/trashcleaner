object FDlgAssoc: TFDlgAssoc
  Left = 322
  Top = 95
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1040#1089#1089#1086#1094#1080#1072#1094#1080#1080
  ClientHeight = 169
  ClientWidth = 313
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 10
    Top = 10
    Width = 117
    Height = 13
    Caption = 'TrashCleaner List (*.mcx)'
  end
  object Shape1: TShape
    Left = 135
    Top = 20
    Width = 171
    Height = 1
  end
  object Label2: TLabel
    Left = 10
    Top = 70
    Width = 129
    Height = 13
    Caption = #1050#1086#1085#1090#1077#1082#1089#1090#1085#1086#1077' '#1084#1077#1085#1102' '#1087#1072#1087#1086#1082
  end
  object Shape2: TShape
    Left = 150
    Top = 80
    Width = 156
    Height = 1
  end
  object Label3: TLabel
    Left = 10
    Top = 35
    Width = 37
    Height = 13
    Caption = #1057#1090#1072#1090#1091#1089':'
  end
  object Label4: TLabel
    Left = 10
    Top = 95
    Width = 37
    Height = 13
    Caption = #1057#1090#1072#1090#1091#1089':'
  end
  object Label5: TLabel
    Left = 50
    Top = 35
    Width = 19
    Height = 13
    Caption = #1053#1077#1090
  end
  object Label6: TLabel
    Left = 50
    Top = 95
    Width = 19
    Height = 13
    Caption = #1053#1077#1090
  end
  object FlatButton1: TFlatButton
    Left = 90
    Top = 30
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1040#1089#1089#1086#1094#1080#1080#1088#1086#1074#1072#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    OnClick = FlatButton1Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clNavy
    FontColorHighLight = clNavy
    FontColorPushedState = clNavy
    FontColorDisabled = clGray
  end
  object FlatButton2: TFlatButton
    Left = 200
    Top = 30
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1059#1076#1072#1083#1080#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
    OnClick = FlatButton2Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clMaroon
    FontColorHighLight = clMaroon
    FontColorPushedState = clMaroon
    FontColorDisabled = clGray
  end
  object FlatButton3: TFlatButton
    Left = 90
    Top = 90
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1040#1089#1089#1086#1094#1080#1080#1088#1086#1074#1072#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    OnClick = FlatButton3Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clNavy
    FontColorHighLight = clNavy
    FontColorPushedState = clNavy
    FontColorDisabled = clGray
  end
  object FlatButton4: TFlatButton
    Left = 200
    Top = 90
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1059#1076#1072#1083#1080#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    OnClick = FlatButton4Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clMaroon
    FontColorHighLight = clMaroon
    FontColorPushedState = clMaroon
    FontColorDisabled = clGray
  end
  object FlatButton5: TFlatButton
    Left = 200
    Top = 136
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'Ok'
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
    OnClick = FlatButton5Click
    ColorHighLight = clWindow
    ButtonType = Border01
    ColorHighLightEnable = True
  end
  object Edit1: TEdit
    Left = 200
    Top = 65
    Width = 106
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 5
    Text = 'Open in TrashCleaner'
  end
end
