object FDlgFolders: TFDlgFolders
  Left = 142
  Top = 212
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1055#1086#1080#1089#1082' '#1074' TEMP-'#1087#1072#1087#1082#1072#1093
  ClientHeight = 218
  ClientWidth = 359
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 3
    Width = 265
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1088#1077#1084#1077#1085#1085#1099#1077' '#1087#1072#1087#1082#1080' '#1080' '#1076#1086#1073#1072#1074#1100#1090#1077' '#1080#1093' '#1074' '#1089#1087#1080#1089#1086#1082
  end
  object ShellTreeView1: TShellTreeView
    Left = 5
    Top = 20
    Width = 166
    Height = 191
    ObjectTypes = [otFolders, otHidden]
    Root = 'rfDesktop'
    UseShellImages = True
    AutoRefresh = False
    Ctl3D = False
    Indent = 19
    ParentColor = False
    ParentCtl3D = False
    RightClickSelect = True
    ShowRoot = False
    TabOrder = 0
  end
  object ListBox1: TListBox
    Left = 180
    Top = 20
    Width = 171
    Height = 97
    Ctl3D = False
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 1
    OnDblClick = ListBox1DblClick
    OnKeyDown = ListBox1KeyDown
  end
  object FlatButton1: TFlatButton
    Left = 180
    Top = 125
    Width = 86
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    OnClick = FlatButton1Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clGreen
    FontColorHighLight = clGreen
    FontColorPushedState = clGreen
    FontColorDisabled = clGray
  end
  object FlatButton2: TFlatButton
    Left = 180
    Top = 186
    Width = 126
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1055#1086#1080#1089#1082
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
    OnClick = FlatButton2Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clNavy
    FontColorHighLight = clNavy
    FontColorPushedState = clNavy
    FontColorDisabled = clGray
  end
  object CheckBox1: TCheckBox
    Left = 180
    Top = 160
    Width = 176
    Height = 17
    Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1086#1082#1085#1086' '#1087#1088#1080' '#1079#1072#1087#1091#1089#1082#1077
    TabOrder = 2
  end
  object FlatButton3: TFlatButton
    Left = 270
    Top = 125
    Width = 81
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1059#1076#1072#1083#1080#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
    OnClick = FlatButton3Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clMaroon
    FontColorHighLight = clMaroon
    FontColorPushedState = clMaroon
    FontColorDisabled = clGray
  end
  object FlatButton4: TFlatButton
    Left = 310
    Top = 186
    Width = 41
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = 'Ok'
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 6
    OnClick = FlatButton4Click
    ColorHighLight = clWindow
    ButtonType = Border01
    ColorHighLightEnable = True
  end
  object FlatButton5: TFlatButton
    Left = 330
    Top = 4
    Width = 21
    Height = 13
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = '?'
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 7
    OnClick = FlatButton5Click
    ColorHighLight = clWindow
    ButtonType = Border01
    ColorHighLightEnable = True
  end
end
