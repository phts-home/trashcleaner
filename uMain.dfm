object FMain: TFMain
  Left = 138
  Top = 100
  Width = 755
  Height = 435
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'TrashCleaner'
  Color = clBtnFace
  Constraints.MinHeight = 435
  Constraints.MinWidth = 755
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnMouseMove = FormMouseMove
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 171
    Top = 5
    Width = 5
    Height = 356
    Cursor = crHSplit
    BevelOuter = bvNone
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 7
    OnMouseDown = Panel1MouseDown
    OnMouseMove = Panel1MouseMove
    OnMouseUp = Panel1MouseUp
  end
  object GroupBox1: TGroupBox
    Left = 635
    Top = 70
    Width = 106
    Height = 291
    Caption = '  '#1048#1089#1082#1072#1090#1100':  '
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 5
    object ChBox_tmp: TCheckBox
      Left = 10
      Top = 20
      Width = 91
      Height = 17
      Caption = '*.tmp'
      Checked = True
      Ctl3D = False
      ParentCtl3D = False
      State = cbChecked
      TabOrder = 0
    end
    object ChBox_thumbs: TCheckBox
      Left = 10
      Top = 40
      Width = 91
      Height = 17
      Caption = 'thumbs.db'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object ChBox_bak: TCheckBox
      Left = 10
      Top = 60
      Width = 91
      Height = 17
      Caption = '*.bak'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object ChBox_tttt: TCheckBox
      Left = 10
      Top = 120
      Width = 91
      Height = 17
      Caption = '*.~*'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object ChBox_ttt: TCheckBox
      Left = 10
      Top = 100
      Width = 91
      Height = 17
      Caption = '~*.*'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object ChBox_old: TCheckBox
      Left = 10
      Top = 80
      Width = 91
      Height = 17
      Caption = '*.old'
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
    object ChBox_log: TCheckBox
      Left = 10
      Top = 240
      Width = 91
      Height = 17
      Caption = '*.log'
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 6
    end
    object ChBox_bbb: TCheckBox
      Left = 10
      Top = 220
      Width = 91
      Height = 17
      Caption = 'backup_of_*.*'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object ChBox_wbk: TCheckBox
      Left = 10
      Top = 200
      Width = 91
      Height = 17
      Caption = '*.wbk'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object ChBox_ddd: TCheckBox
      Left = 10
      Top = 160
      Width = 91
      Height = 17
      Caption = '$*.*'
      Checked = True
      State = cbChecked
      TabOrder = 9
    end
    object ChBox_swp: TCheckBox
      Left = 10
      Top = 140
      Width = 91
      Height = 17
      Caption = '*.swp'
      Checked = True
      Ctl3D = False
      ParentCtl3D = False
      State = cbChecked
      TabOrder = 10
    end
    object ChBox_ccc: TCheckBox
      Left = 10
      Top = 260
      Width = 19
      Height = 17
      Checked = True
      State = cbChecked
      TabOrder = 11
    end
    object ChBox_dddd: TCheckBox
      Left = 10
      Top = 180
      Width = 91
      Height = 17
      Caption = '*.$*'
      Checked = True
      State = cbChecked
      TabOrder = 12
    end
    object FlatButton3: TFlatButton
      Left = 30
      Top = 260
      Width = 66
      Height = 16
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = #1057#1086#1073#1089#1090#1074'.'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 13
      OnClick = FlatButton3Click
      ColorHighLight = clWindow
      ColorDisabled = clBtnFace
      ButtonType = Border01
      ColorHighLightEnable = True
      FontColorNormalState = clBlack
      FontColorHighLight = clBlack
      FontColorPushedState = clBlack
      FontColorDisabled = clGray
    end
    object FlatButton4: TFlatButton
      Left = 70
      Top = 0
      Width = 16
      Height = 16
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = 'A'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 14
      OnClick = aCheckboxesAllExecute
      ColorHighLight = clWindow
      ColorDisabled = clBtnFace
      ButtonType = Border01
      ColorHighLightEnable = True
      FontColorNormalState = clBlack
      FontColorHighLight = clBlack
      FontColorPushedState = clBlack
      FontColorDisabled = clGray
    end
    object FlatButton5: TFlatButton
      Left = 87
      Top = 0
      Width = 16
      Height = 16
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = 'N'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 15
      OnClick = aCheckboxesNoneExecute
      ColorHighLight = clWindow
      ColorDisabled = clBtnFace
      ButtonType = Border01
      ColorHighLightEnable = True
      FontColorNormalState = clBlack
      FontColorHighLight = clBlack
      FontColorPushedState = clBlack
      FontColorDisabled = clGray
    end
  end
  object ShellTreeView1: TShellTreeView
    Left = 5
    Top = 30
    Width = 166
    Height = 331
    ObjectTypes = [otFolders, otHidden]
    Root = 'rfDesktop'
    UseShellImages = True
    AutoRefresh = False
    Ctl3D = False
    HideSelection = False
    Indent = 19
    ParentColor = False
    ParentCtl3D = False
    RightClickSelect = True
    ShowRoot = False
    TabOrder = 2
    OnChange = ShellTreeView1Change
  end
  object Edit1: TEdit
    Left = 5
    Top = 5
    Width = 166
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    OnChange = Edit1Change
    OnKeyPress = Edit1KeyPress
  end
  object FlatButton1: TFlatButton
    Left = 635
    Top = 5
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1055#1086#1080#1089#1082
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    OnClick = FlatButton1Click
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clNavy
    FontColorHighLight = clNavy
    FontColorPushedState = clNavy
    FontColorDisabled = clGray
  end
  object FlatButton2: TFlatButton
    Left = 635
    Top = 35
    Width = 106
    Height = 25
    BevelOuter = bvNone
    BorderStyle = bsSingle
    Caption = #1059#1076#1072#1083#1080#1090#1100
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 4
    OnClick = aDelFileExecute
    ColorHighLight = clWindow
    ColorDisabled = clBtnFace
    ButtonType = Border01
    ColorHighLightEnable = True
    FontColorNormalState = clMaroon
    FontColorHighLight = clMaroon
    FontColorPushedState = clMaroon
    FontColorDisabled = clGray
  end
  object ListView1: TListView
    Left = 176
    Top = 5
    Width = 454
    Height = 356
    Checkboxes = True
    Columns = <
      item
        Caption = #1060#1072#1081#1083
        Width = 317
      end
      item
        Caption = #1056#1072#1079#1084#1077#1088
      end
      item
        Caption = #1057#1090#1072#1090#1091#1089
        Width = 53
      end>
    ColumnClick = False
    Ctl3D = False
    HideSelection = False
    MultiSelect = True
    ReadOnly = True
    PopupMenu = PopupMenu1
    TabOrder = 0
    ViewStyle = vsReport
    OnDblClick = ListView1DblClick
    OnMouseDown = ListView1MouseDown
    OnMouseMove = ListView1MouseMove
    OnMouseUp = ListView1MouseUp
    OnResize = ListView1Resize
    OnSelectItem = ListView1SelectItem
  end
  object StatusBar1: TStatusBar
    Left = 5
    Top = 365
    Width = 691
    Height = 19
    Align = alCustom
    Panels = <
      item
        Width = 317
      end
      item
        Alignment = taRightJustify
        Bevel = pbNone
        Text = 'All:'
        Width = 30
      end
      item
        Alignment = taCenter
        Text = '0/0/0 b'
        Width = 90
      end
      item
        Alignment = taRightJustify
        Bevel = pbNone
        Text = 'Sel:'
        Width = 30
      end
      item
        Alignment = taCenter
        Text = '0/0/0 b'
        Width = 90
      end
      item
        Alignment = taRightJustify
        Bevel = pbNone
        Text = 'Del:'
        Width = 30
      end
      item
        Alignment = taCenter
        Text = '0/0/0 b'
        Width = 90
      end
      item
        Bevel = pbNone
        Width = 20
      end>
    ParentShowHint = False
    ShowHint = True
    OnMouseDown = StatusBar1MouseDown
    OnMouseMove = StatusBar1MouseMove
  end
  object MainMenu1: TMainMenu
    Left = 10
    Top = 5
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object N15: TMenuItem
        Action = aSearch
      end
      object N33: TMenuItem
        Action = aSearchTemp
      end
      object N29: TMenuItem
        Action = aStop
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object N5: TMenuItem
        Action = aDelFile
      end
      object N17: TMenuItem
        Action = aDelFilesAll
      end
      object N53: TMenuItem
        Caption = '-'
      end
      object N52: TMenuItem
        Action = aClearRec
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object N6: TMenuItem
        Action = aExit
      end
    end
    object N27: TMenuItem
      Caption = #1042#1099#1073#1086#1088
      object N31: TMenuItem
        Action = aSelAll
        Caption = #1042#1089#1105
      end
      object N28: TMenuItem
        Action = aSelNone
      end
      object N32: TMenuItem
        Action = aSelInv
      end
    end
    object N30: TMenuItem
      Caption = #1057#1087#1080#1089#1086#1082
      object N42: TMenuItem
        Action = aListOpen
      end
      object N41: TMenuItem
        Action = aListSave
      end
      object N43: TMenuItem
        Caption = '-'
      end
      object N19: TMenuItem
        Action = aDelItem
      end
      object N7: TMenuItem
        Action = aDelItemsAll
      end
    end
    object N8: TMenuItem
      Caption = #1055#1088#1086#1075#1088#1072#1084#1084#1072
      object N38: TMenuItem
        Caption = #1048#1085#1090#1077#1088#1092#1077#1081#1089
        object N25: TMenuItem
          Action = aInterfGrid
          AutoCheck = True
        end
        object N12: TMenuItem
          Action = aInterfFlatScrb
          AutoCheck = True
        end
        object N16: TMenuItem
          Action = aInterfSelRow
          AutoCheck = True
        end
        object N45: TMenuItem
          Caption = '-'
        end
        object N35: TMenuItem
          Action = aInterfSizing
        end
      end
      object N39: TMenuItem
        Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1080
        object N40: TMenuItem
          Action = aOptUseRec
          AutoCheck = True
        end
        object N9: TMenuItem
          Action = aOptSubFold
          AutoCheck = True
        end
        object N10: TMenuItem
          Action = aOptDelSuc
          AutoCheck = True
        end
        object N11: TMenuItem
          Action = aOptClBefSearch
          AutoCheck = True
        end
        object N51: TMenuItem
          Caption = '-'
        end
        object N47: TMenuItem
          Action = aOptReset
        end
      end
      object N34: TMenuItem
        Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086#1080#1089#1082#1072
        object N50: TMenuItem
          Action = aFilterFF
        end
        object N49: TMenuItem
          Action = aFilterFiles
        end
        object N48: TMenuItem
          Action = aFilterFold
        end
      end
      object N36: TMenuItem
        Caption = '-'
      end
      object N46: TMenuItem
        Action = aShowFDlgAssoc
      end
      object EMP1: TMenuItem
        Action = aShowFDlgFolder
      end
    end
    object N3: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1082#1072
      object ReadMetxt1: TMenuItem
        Action = aShowReadMetxt
      end
      object N44: TMenuItem
        Caption = '-'
      end
      object N4: TMenuItem
        Action = aShowFDlgAbout
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 40
    Top = 5
    object N20: TMenuItem
      Action = aDelFile
    end
    object N22: TMenuItem
      Action = aDelFilesAll
    end
    object N24: TMenuItem
      Caption = '-'
    end
    object N21: TMenuItem
      Action = aDelItem
    end
    object N23: TMenuItem
      Action = aDelItemsAll
    end
    object N26: TMenuItem
      Caption = '-'
    end
    object N18: TMenuItem
      Action = aFileOpen
    end
    object N13: TMenuItem
      Action = aFileInFolder
    end
    object N37: TMenuItem
      Action = aFileProp
      Default = True
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = 'TrashCleaner List (*.mcx)|*.mcx'
    Left = 75
    Top = 5
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'mcx'
    Filter = 'TrashCleaner List (*.mcx)|*.mcx'
    Left = 105
    Top = 5
  end
  object ActionList1: TActionList
    Left = 140
    Top = 5
    object aSearch: TAction
      Category = #1060#1072#1081#1083
      Caption = #1055#1086#1080#1089#1082
      ShortCut = 120
      OnExecute = aSearchExecute
    end
    object aOptUseRec: TAction
      Category = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      AutoCheck = True
      Caption = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1090#1100' '#1082#1086#1088#1079#1080#1085#1091
      OnExecute = aOptUseRecExecute
    end
    object aSearchTemp: TAction
      Category = #1060#1072#1081#1083
      Caption = #1055#1086#1080#1089#1082' *.* '#1074' TEMP-'#1087#1072#1087#1082#1072#1093
      ShortCut = 16504
      OnExecute = aSearchTempExecute
    end
    object aStop: TAction
      Category = #1060#1072#1081#1083
      Caption = #1054#1089#1090#1072#1085#1086#1074#1080#1090#1100
      Enabled = False
      ShortCut = 27
      OnExecute = aStopExecute
    end
    object aDelFile: TAction
      Category = #1060#1072#1081#1083
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1092#1072#1081#1083'('#1099')'
      ShortCut = 16430
      OnExecute = aDelFileExecute
    end
    object aDelFilesAll: TAction
      Category = #1060#1072#1081#1083
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1089#1105
      OnExecute = aDelFilesAllExecute
    end
    object aClearRec: TAction
      Category = #1060#1072#1081#1083
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1082#1086#1088#1079#1080#1085#1091
      OnExecute = aClearRecExecute
    end
    object aExit: TAction
      Category = #1060#1072#1081#1083
      Caption = #1042#1099#1093#1086#1076
      ShortCut = 32883
      OnExecute = aExitExecute
    end
    object aListOpen: TAction
      Category = #1057#1087#1080#1089#1086#1082
      Caption = #1054#1090#1082#1088#1099#1090#1100'...'
      ShortCut = 16463
      OnExecute = aListOpenExecute
    end
    object aListSave: TAction
      Category = #1057#1087#1080#1089#1086#1082
      Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082'...'
      ShortCut = 16467
      OnExecute = aListSaveExecute
    end
    object aDelItem: TAction
      Category = #1057#1087#1080#1089#1086#1082
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1091#1085#1082#1090'('#1099') '#1080#1079' '#1089#1087#1080#1089#1082#1072
      ShortCut = 46
      OnExecute = aDelItemExecute
    end
    object aDelItemsAll: TAction
      Category = #1057#1087#1080#1089#1086#1082
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1089#1087#1080#1089#1086#1082
      OnExecute = aDelItemsAllExecute
    end
    object aSelAll: TAction
      Category = #1042#1099#1073#1086#1088
      Caption = #1042#1099#1076#1077#1083#1080#1090#1100' '#1074#1089#1105
      ShortCut = 16449
      OnExecute = aSelAllExecute
    end
    object aShowFDlgAssoc: TAction
      Category = #1055#1088#1086#1075#1088#1072#1084#1084#1072
      Caption = #1040#1089#1089#1086#1094#1080#1072#1094#1080#1080'...'
      OnExecute = aShowFDlgAssocExecute
    end
    object aShowFDlgFolder: TAction
      Category = #1055#1088#1086#1075#1088#1072#1084#1084#1072
      Caption = #1055#1086#1080#1089#1082' '#1074' TEMP-'#1087#1072#1087#1082#1072#1093'...'
      OnExecute = aShowFDlgFolderExecute
    end
    object aShowReadMetxt: TAction
      Category = #1057#1087#1088#1072#1074#1082#1072
      Caption = 'ReadMe.txt'
      OnExecute = aShowReadMetxtExecute
    end
    object aShowFDlgAbout: TAction
      Category = #1057#1087#1088#1072#1074#1082#1072
      Caption = #1054' '#1087#1088#1086#1075#1088#1072#1084#1084#1077'...'
      OnExecute = aShowFDlgAboutExecute
    end
    object aInterfGrid: TAction
      Category = #1048#1085#1090#1077#1088#1092#1077#1081#1089
      AutoCheck = True
      Caption = #1057#1077#1090#1082#1072
      OnExecute = aInterfGridExecute
    end
    object aInterfFlatScrb: TAction
      Category = #1048#1085#1090#1077#1088#1092#1077#1081#1089
      AutoCheck = True
      Caption = #1055#1083#1086#1089#1082#1080#1077' '#1087#1086#1083#1086#1089#1099' '#1087#1088#1086#1082#1088#1091#1090#1082#1080
      OnExecute = aInterfFlatScrbExecute
    end
    object aInterfSelRow: TAction
      Category = #1048#1085#1090#1077#1088#1092#1077#1081#1089
      AutoCheck = True
      Caption = #1042#1099#1076#1077#1083#1103#1090#1100' '#1074#1089#1102' '#1089#1090#1088#1086#1082#1091
      OnExecute = aInterfSelRowExecute
    end
    object aInterfSizing: TAction
      Category = #1048#1085#1090#1077#1088#1092#1077#1081#1089
      Caption = #1048#1089#1093#1086#1076#1085#1099#1081' '#1088#1072#1079#1084#1077#1088
      OnExecute = aInterfSizingExecute
    end
    object aOptSubFold: TAction
      Category = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      AutoCheck = True
      Caption = #1055#1086#1080#1089#1082' '#1074#1086' '#1074#1083#1086#1078#1077#1085#1085#1099#1093' '#1087#1072#1087#1082#1072#1093
      OnExecute = aOptSubFoldExecute
    end
    object aOptDelSuc: TAction
      Category = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      AutoCheck = True
      Caption = #1059#1076#1072#1083#1103#1090#1100' '#1091#1089#1087#1077#1096#1085#1086#1077' '#1079#1072#1076#1072#1085#1080#1077
      OnExecute = aOptDelSucExecute
    end
    object aOptClBefSearch: TAction
      Category = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      AutoCheck = True
      Caption = #1054#1095#1080#1097#1072#1090#1100' '#1089#1087#1080#1089#1086#1082' '#1087#1077#1088#1077#1076' '#1087#1086#1080#1089#1082#1086#1084
      OnExecute = aOptClBefSearchExecute
    end
    object aOptReset: TAction
      Category = #1053#1072#1089#1090#1088#1086#1081#1082#1080
      Caption = #1054#1073#1085#1091#1083#1080#1090#1100' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1091#1076#1072#1083#1077#1085#1085#1099#1093' '#1092#1072#1081#1083#1086#1074'/'#1087#1072#1087#1086#1082
      OnExecute = aOptResetExecute
    end
    object aFilterFF: TAction
      Category = #1060#1080#1083#1100#1090#1088' '#1087#1086#1080#1089#1082#1072
      Caption = #1060#1072#1081#1083#1099' '#1080' '#1087#1072#1087#1082#1080
      OnExecute = aFilterFFExecute
    end
    object aFilterFiles: TAction
      Category = #1060#1080#1083#1100#1090#1088' '#1087#1086#1080#1089#1082#1072
      Caption = #1058#1086#1083#1100#1082#1086' '#1092#1072#1081#1083#1099
      OnExecute = aFilterFilesExecute
    end
    object aFilterFold: TAction
      Category = #1060#1080#1083#1100#1090#1088' '#1087#1086#1080#1089#1082#1072
      Caption = #1058#1086#1083#1100#1082#1086' '#1087#1072#1087#1082#1080
      OnExecute = aFilterFoldExecute
    end
    object aCheckboxesAll: TAction
      Category = #1043#1072#1083#1086#1095#1082#1080
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1074#1089#1077
      OnExecute = aCheckboxesAllExecute
    end
    object aCheckboxesNone: TAction
      Category = #1043#1072#1083#1086#1095#1082#1080
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1085#1080#1095#1077#1075#1086
      OnExecute = aCheckboxesNoneExecute
    end
    object aFileOpen: TAction
      Category = 'Popup'
      Caption = #1054#1090#1082#1088#1099#1090#1100
      ShortCut = 115
      OnExecute = aFileOpenExecute
    end
    object aFileInFolder: TAction
      Category = 'Popup'
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1089#1086#1076#1077#1088#1078#1072#1097#1091#1102' '#1086#1073#1098#1077#1082#1090' '#1087#1072#1087#1082#1091
      ShortCut = 16499
      OnExecute = aFileInFolderExecute
    end
    object aFileProp: TAction
      Category = 'Popup'
      Caption = #1057#1074#1086#1081#1089#1090#1074#1072
      OnExecute = aFilePropExecute
    end
    object aSelNone: TAction
      Category = #1042#1099#1073#1086#1088
      Caption = #1053#1080#1095#1077#1075#1086
      ShortCut = 16452
      OnExecute = aSelNoneExecute
    end
    object aSelInv: TAction
      Category = #1042#1099#1073#1086#1088
      Caption = #1048#1085#1074#1077#1088#1090#1080#1088#1086#1074#1072#1090#1100
      ShortCut = 16457
      OnExecute = aSelInvExecute
    end
  end
end
