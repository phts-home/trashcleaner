unit uSearch;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, FlatButton, Gauges, StdCtrls;

type
  TFDlgSearch = class(TForm)
    FlatButton1: TFlatButton;
    Panel1: TPanel;
    Gauge1: TGauge;
    FlatButton2: TFlatButton;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    GroupBox3: TGroupBox;
    Memo1: TMemo;
    Panel2: TPanel;
    CheckBox1: TCheckBox;
    procedure FlatButton1Click(Sender: TObject);
    procedure FlatButton2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDlgSearch: TFDlgSearch;

implementation

uses uMain;

{$R *.dfm}

procedure TFDlgSearch.FlatButton1Click(Sender: TObject);
begin
FMain.aStop.Execute;
end;

procedure TFDlgSearch.FlatButton2Click(Sender: TObject);
begin
Close;
end;

procedure TFDlgSearch.FormCreate(Sender: TObject);
begin
CheckBox1.Checked:=FMain.cfg.ReadBool('Program','CloseSearchWin',False)
end;

procedure TFDlgSearch.FormShow(Sender: TObject);
begin
Left:=(2*FMain.Left+FMain.Width)div 2 - Width div 2;
Top:=(2*FMain.Top+FMain.Height)div 2 - Height div 2;
end;

end.
